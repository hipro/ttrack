# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import argparse
from pyramid.paster import bootstrap

def get_all_keys_from_timesheet_map(ts_map):
    keys = []
    for u, ts in ts_map.iteritems():
        tl_all = ts.get_all()
        keys.extend(list(tl_all.get_ute_keys(None, None)))
    return keys

def get_all_keys_from_timelog_map(tl_map):
    keys = []
    for u, tl in tl_map.iteritems():
        keys.extend(list(tl.get_ute_keys(None, None)))
    return keys

def dump_timeentry(tl, key):
    te = tl.get_ute(key)
    print ("\t User: %s, Project : %s, Client : %s" %
           (te.user.userid, te.project.project_id, te.project.client.client_id))

def match_level_one_all_entries(root):
    ts = root['timesheet']
    tl_all = ts.get_all_timelog()
    all_keys = list(tl_all.get_ute_keys(None, None))
    user_keys = get_all_keys_from_timesheet_map(ts.get_user_map())
    project_keys = get_all_keys_from_timesheet_map(ts.get_project_map())
    client_keys = get_all_keys_from_timesheet_map(ts.get_client_map())

    for key in all_keys:
        if key not in user_keys:
            print ("\nKey : %s not found in UserTimesheet" % key)
            dump_timeentry(tl_all, key)
        if key not in project_keys:
            print ("Key : %s not found in ProjectTimesheet" % key)
            dump_timeentry(tl_all, key)
        if key not in client_keys:
            print ("Key : %s not found in ClientTimesheet" % key)
            dump_timeentry(tl_all, key)

    for key in project_keys:
        if key not in client_keys:
            print ("Key : %s found in ProjectTimesheet but not in Client" % key)


def main(config):
    env = bootstrap(config)
    root = env['root']
    match_level_one_all_entries(root)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("config", type=str, help="Config INI file")
    args = parser.parse_args()
    main(args.config)

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import argparse
from pyramid.paster import bootstrap
import transaction

# Note: This will only fix the case where data was lost by adding a
# 'user' as a member to a project.
#
# If a user was removed from a project membership, the bug causes a
# 'user removed' scenario to happen thereby deleting all data
# associated with the user, leaving no way to recover the lost data.

def main(config):
    env = bootstrap(config)
    root = env['root']
    all_tl = root['timesheet'].get_all_timelog()
    all_keys = all_tl.get_ute_keys(None, None)
    count = 1
    for ute_key in all_keys:
        ute = all_tl.get_ute(ute_key)
        u_tl = root['timesheet'].get_user_timelog(ute.user)
        if ute_key not in u_tl.get_ute_keys(None, None):
            u_ts = root['timesheet'].get_user_timesheet(ute.user)
            u_ts.add(ute)
            print("%02d : added '%s' to '%s'" %
                  (count, ute_key, ute.user.userid))
            count += 1
    transaction.commit()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("config", type=str, help="Config INI file")
    args = parser.parse_args()
    main(args.config)

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope import component
from repoze.folder.interfaces import (
    IObjectWillBeRemovedEvent,
)

from pyramid.traversal import find_root
from ttrack.projects.interfaces import IProjectWillBeRemovedEvent
from ttrack.interfaces import IApplicationWillBeCreatedEvent
from .models import ClientContainer

@component.adapter(IApplicationWillBeCreatedEvent)
def initialize_db(event):
    event.app['clients'] = ClientContainer()

@component.adapter(IProjectWillBeRemovedEvent)
def handle_project_remove(event):
    project = event.project
    client = project.client
    log.debug('clients: project-remove %s', project.project_id)
    client.remove_project(project)

def register_zca_subscribers():
    component.provideHandler(initialize_db)
    component.provideHandler(handle_project_remove)

register_zca_subscribers()

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    )

# cornice view error handlers

def body_client_id_exists(request):
    clients = request.root['clients']
    client_id = request.json.get("client_id", None)
    if client_id and client_id not in clients:
        request.errors.add(
            'body', 'client_id', "Client='%s' does not exist" % client_id)
        request.errors.status = HTTPBadRequest.code

def qs_client_id_exists(request):
    clients = request.root['clients']
    client_id = request.params.get("client_id", None)
    if client_id and client_id not in clients:
        request.errors.add('querystring', 'client_id',
                           "Client='%s' does not exist" % client_id)
        request.errors.status = HTTPBadRequest.code

def url_client_id_exists(request):
    clients = request.root['clients']
    client_id = request.matchdict["client_id"]
    if client_id not in clients:
        request.errors.add(
            'url', 'client_id', "Client='%s' does not exist" % client_id)
        request.errors.status = HTTPNotFound.code

def strict_url_client_id_exists(request):
    # This method will be used for validators in 'delete' request of
    # REST as the delete of non-existant client id is bad request
    clients = request.root['clients']
    client_id = request.matchdict["client_id"]
    if client_id not in clients:
        request.errors.add(
            'url', 'client_id', "Client='%s' does not exist" % client_id)
        request.errors.status = HTTPBadRequest.code


# pyramid view error handlers

def pv_url_client_id_exists(request):
    clients = request.root['clients']
    client_id = request.matchdict["client_id"]
    if client_id not in clients:
        raise HTTPNotFound("Client='%s' does not exist" % client_id)

def pv_qs_client_id_exists(request):
    clients = request.root['clients']
    client_id = request.params.get("client_id", None)
    if client_id and client_id not in clients:
        raise HTTPBadRequest("Client='%s' does not exist" % client_id)

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from persistent import Persistent
from persistent.mapping import PersistentMapping
from BTrees.OOBTree import OOTreeSet

from zope import interface, event
from repoze import folder

from ttrack import errors
from .interfaces import IClient, IClientAddedEvent, IClientWillBeRemovedEvent

@interface.implementer(IClientAddedEvent)
class ClientAddedEvent(object):

    def __init__(self, client):
        self.client = client

@interface.implementer(IClientWillBeRemovedEvent)
class ClientWillBeRemovedEvent(object):

    def __init__(self, client):
        self.client = client

class ClientContainer(folder.Folder):

    def add(self, client):
        super(ClientContainer, self).add(
            client.client_id, client, send_events=False)
        event.notify(ClientAddedEvent(client))

    def is_id_unique(self, client_id):
        return client_id not in self

    def get(self, client_id, *args):
        client_id = client_id.strip().lower()
        return super(ClientContainer, self).get(client_id, *args)

    def __contains__(self, client_id):
        client_id = client_id.strip().lower()
        return super(ClientContainer, self).__contains__(client_id)

    def remove(self, client_id):
        client_id = client_id.strip().lower()
        client = self.get(client_id)
        event.notify(ClientWillBeRemovedEvent(client))
        return super(ClientContainer, self).remove(client_id, send_events=False)

    def rename_client_id(self, old_id, new_id):
        """This method will be exclusively used by the evolution script to
        migrate client ids which contains upper-case letters in them.
        """
        client = super(ClientContainer, self).get(old_id, None)
        assert client is not None, "Old client id (%s) does not exist" % old_id
        assert new_id not in self, "New client id (%s) already exists" % new_id
        client_id = new_id.strip().lower()
        client.client_id = client_id
        # Don't propogate the IObjectWillBeRemovedEvent and
        # IObjectAddedEvent
        super(ClientContainer, self).remove(old_id, send_events=False)
        super(ClientContainer, self).add(client_id, client, send_events=False)


@interface.implementer(IClient)
class Client(Persistent):

    def __init__(self, client_id, name, address=None):
        super(Client, self).__init__()
        self.client_id = client_id.strip().lower()
        self.name = name.strip()
        self.address = address
        self.projects = OOTreeSet()

    def to_dict(self, exclude_projects=False):
        data = {
            'client_id': self.client_id,
            'name' : self.name,
            'address' : self.address,
        }
        if exclude_projects is False:
            data['projects'] = [p.to_dict() for p in self.projects]
        return data

    @classmethod
    def from_dict(klass, data):
        client = klass(client_id=data['client_id'], name=data['name'])
        client.address = data.get('address', None)
        if 'projects' in data:
            for project in data['projects']:
                client.projects.add(project)
        return client

    def add_project(self, project):
        self.projects.add(project)

    def remove_project(self, project):
        self.projects.remove(project)

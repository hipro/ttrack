# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from persistent import Persistent
from persistent.mapping import PersistentMapping
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )

from zope import interface, event
from repoze import folder
from BTrees.OOBTree import OOTreeSet

from ttrack import errors
from ttrack.auth import AUTH_PERMISSIONS
from .interfaces import IUser, IUserWillBeRemovedEvent, IUserAddedEvent

USER_PERMISSIONS = ("account.modify",)

@interface.implementer(IUserAddedEvent)
class UserAddedEvent(object):

    def __init__(self, user):
        self.user = user

@interface.implementer(IUserWillBeRemovedEvent)
class UserWillBeRemovedEvent(object):

    def __init__(self, user):
        self.user = user

class UserContainer(folder.Folder):

    def is_email_unique(self, email):
        user = self.find_with_email_id(email)
        return user is None

    def is_userid_unique(self, userid):
        return userid not in self

    def add(self, user):
        super(UserContainer, self).add(user.userid, user, send_events=False)
        event.notify(UserAddedEvent(user))

    def get(self, userid, *args):
        userid = userid.strip().lower()
        return super(UserContainer, self).get(userid, *args)

    def __contains__(self, userid):
        userid = userid.strip().lower()
        return super(UserContainer, self).__contains__(userid)

    def remove(self, userid):
        user = self.get(userid)
        if user.is_admin():
            raise errors.BadParameter(
                "User=%s is an system administrator. Cannot delete." % \
                (userid))
        event.notify(UserWillBeRemovedEvent(user))
        super(UserContainer, self).remove(
            userid.strip().lower(), send_events=False)

    def find_with_email_id(self, email_id):
        for user in self.values():
            if user.email_id == email_id:
                return user
        return None

    def find_and_replace_roles(self, old_role, new_role):
        for user in self.values():
            if user.role == old_role:
                user.role = new_role

@interface.implementer(IUser)
class User(Persistent):

    def __init__(self, userid=None, real_name=None, password=None,
                 email_id=None):
        super(User, self).__init__()
        self.userid = userid.strip().lower()
        self.real_name = real_name.strip()
        self.email_id = email_id
        self.password = password
        self.role = None
        self.active = True
        self.projects = OOTreeSet()
        self.__acl__ = [(Allow, self.userid, USER_PERMISSIONS),]

    def check_password(self, passwd):
        return self.active and self.password == passwd

    def to_dict(self, exclude_projects=False):
        data = {
            'userid'    : self.userid,
            'real_name' : self.real_name,
            'email_id' : self.email_id,
            'active' : self.active,
        }
        role_id = ''
        role_name = ''
        if self.role:
            role_id = self.role.role_id
            role_name = self.role.name
        data['role_id'] = role_id
        data['role_name'] = role_name
        if exclude_projects is False:
            data['projects'] = []
            data['clients'] = []
            client_ids = []
            for p in self.projects:
                data['projects'].append(p.to_dict())
                if p.client.client_id not in client_ids:
                    client_ids.append(p.client.client_id)
                    data['clients'].append(p.client.to_dict(
                        exclude_projects=True))
        return data

    @classmethod
    def from_dict(klass, data):
        user = klass(userid=data['userid'], real_name=data['real_name'],
                     email_id=data['email_id'], password=data['password'])
        return user

    def is_admin(self):
        return self.userid == 'admin'

    def add_project(self, project):
        self.projects.add(project)

    def remove_project(self, project):
        self.projects.remove(project)

    def get_permissions(self):
        if self.userid == "admin":
            return ("ALL_PERMISSIONS",)
        perm = []
        perm.extend(USER_PERMISSIONS)
        perm.extend(AUTH_PERMISSIONS)
        if self.role:
            perm.extend(self.role.get_permissions())
        return perm

def find_user(root, userid):
    users = root['users']
    user = users.get(userid, None)
    return user

def find_group(userid, request):
    root = request.root
    users = root['users']
    user = users.get(userid, None)
    grps = []
    if user:
        if user.role:
            grps.extend(user.role.to_group())
        for p in user.projects:
            grps.extend(p.get_group(user))
    return grps

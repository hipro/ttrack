# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import argparse, os

from repoze.evolution import ZODBEvolutionManager, evolve_to_latest

from pyramid.paster import (
    setup_logging,
    bootstrap,
    )

INITIAL_DB_VERSION = 0
CURRENT_SW_VERSION = 5 # Bump this number (X) and add evolveX.py to
                       # add new evolve script

def main():
    desc = """
    Evolves new database with changes from scripts in evolve packages:
    with no arguments, evolve displays finished and unfinished steps"""
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        'config_uri', help="Configuration URI filename", type=str)
    parser.add_argument('-u', '--upgrade', action="store_true",
                        help="Upgrade to the latest DB version")
    args = parser.parse_args()
    fname = args.config_uri
    if os.path.isfile(fname) is False:
        parser.error("Invalid filename (%s) for configuration URI" % fname)

    # arguments are valid so get 'root' object and setup evolution
    # manager
    setup_logging(fname)
    env = bootstrap(fname)
    root = env['root']
    manager = ZODBEvolutionManager(root, 'ttrack.evolution',
                                   CURRENT_SW_VERSION, INITIAL_DB_VERSION)
    db_ver = manager.get_db_version()
    sw_ver = manager.get_sw_version()
    if args.upgrade is False:
        print ("Current DB version is : %d" % db_ver)
        print ("Current SW version is : %d" % sw_ver)
        if sw_ver > db_ver:
            print ("Database needs %d evolution(s) to upgrade to the "
                   "latest version" % (sw_ver - db_ver))
        else:
            print ("Database already upgraded to the latest version")
        return

    if sw_ver > db_ver:
        evolve_to_latest(manager)
    else:
        print ("No evolution steps performed.")
        print ("Database already upgraded to the latest version")

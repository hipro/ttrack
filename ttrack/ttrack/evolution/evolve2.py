# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from ttrack.errors import BadOperation

def evolve(root):
    """This evolve converts all the role IDs to lower case

    This doesn't resolve conflicts, while trying to rename an
    upper-case role_id to a lower-case, if there is already a role_id
    with that same name (in lower-case) then it raises an error and stops

    So, in order to fix that, the user has to manually delete the
    lower-case role_id from the UI.

    Note: He cannot delete the upper-case role_id even if he wants to
    as the delete request for upper-case role_id will be converted to
    lower-case in the backend eventually only the lower case role_id
    will be deleted
    """
    log.info("Evolving to step 2: convert all role IDs to lower case")
    roles = root['roles']

    to_change = []
    for k in roles.keys():
        if k.islower() is False:
            to_change.append(k)

    for role_id in to_change:
        # This check will convert the role_id to lower case and then
        # verify the container, as the __contains__ method of
        # RoleContainer is overridden
        if role_id in roles:
            log.error("Conflict found, cannot migrate ZODB to evolve step 2")
            log.error("Delete the lower-case role ID (%s) and re-run tt_evolve",
                      role_id.strip().lower())
            raise BadOperation(
                "Renaming role id (%s) conflicts with existing role id (%s)"
                % (role_id, role_id.strip().lower()))
        roles.rename_role_id(role_id, role_id.strip().lower())

    # Only when the permissions for a role is changed
    # IObjectModifiedEvent will be fired, so we explicitly set the ACL
    # on the root object to have an effect on the new role_id
    root.set_acl(roles.get_acl())
    if to_change:
        log.info("Migrated %d record(s) to 'step 2' standard", len(to_change))
    else:
        log.info("All records comply to 'step 2' standard")
    log.info("ZODB successfully evolved to step 2")

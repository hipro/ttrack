# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from ttrack.projects.models import Member

def evolve(root):
    """This evolve adds ProjectRole and Member to project

    Prior to this, there was no concept of project-role and the
    members of a project were maintained in MemberContainer just as an
    User object. Now Member object is introducted with User and
    ProjectRole information attached to it.

    Previously only ("activities.add", "timesheet.add") permissions
    were attached to all members. But, now a whole new fine grained
    permission list is appended based on the project-role for each
    member

    """
    log.info("Evolving to step 5: Introduce ProjectRoles and convert members")
    projects = root['projects']

    for p in projects.values():
        p.fix_acl()
        to_fix = []
        for mem in p.members.values():
            if not isinstance(mem, Member):
                to_fix.append(mem)
        if to_fix:
            log.info("Project (%s): Fixing membership for %d user(s)",
                     p.project_id, len(to_fix))
        for user in to_fix:
            if user.role and 'Manage projects' in user.role.permissions:
                project_role = p.roles.get('manager')
            else:
                project_role = p.roles.get('member')
            p.members.convert_user_to_member_object(user, project_role)

    log.info("ZODB successfully evolved to step 5")

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.security import (
    authenticated_userid,
    )
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden
    )

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )
import colander

from .models import Tag
from . import validators


class TagSchema(colander.MappingSchema):
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")


@cornice_resource(
    name="api.tags",
    collection_path="/api/v1/tags",
    path="/api/v1/tags/{tag_name}.json",
    permission="tags.manage",
    )
class API(object):
    def __init__(self, request):
        self.request = request
        self.tags = request.root['tags']

    # Helper methods
    def get_value(self, key):
        charset = getattr(self.request, 'charset', 'utf-8')
        return self.request.params[key].decode(charset).strip()

    def verify_tag_name_unique(self, request):
        tag_name = request.json.get('name', None)
        # we should check only when there is a 'tag_name'. missing
        # fields are handled by schema validation.
        if tag_name and self.tags.exists(tag_name):
            self.request.errors.add(
                'body', 'name', 'Tag=%s already exists' % (tag_name,))
            self.request.errors.status = HTTPBadRequest.code

    # Views
    @cornice_view(permission="tags.view")
    def collection_get(self):
        data = {
            "type" : 'tag_list',
            "result" : None,
            }
        data['result'] = [tag.to_dict()
                          for tag in self.tags.values()]
        return data

    @cornice_view(schema=TagSchema,
                  validators=('verify_tag_name_unique',),
                  permission="tags.add")
    def collection_post(self):
        tag = Tag(self.request.json['name'])
        self.tags.add(tag)
        data = {
            'type' : 'tag',
            'result' : tag.to_dict()
        }
        return data

    @cornice_view(permission="tags.view",
                  validators=(validators.url_tag_name_exists,))
    def get(self):
        tag_name = self.request.matchdict['tag_name']
        tag = self.tags.get(tag_name)
        data = {
            'type' : "tag",
            'result' : tag.to_dict(),
            }
        return data

    @cornice_view(schema=TagSchema,
                  validators=(validators.url_tag_name_exists,))
    def put(self):
        tag_name = self.request.matchdict['tag_name']

        # ensure that the new tag_name is unique
        new_tag_name = self.request.json['name']
        if ((new_tag_name.lower() != tag_name.lower()) and
            self.tags.exists(new_tag_name)):
            self.request.errors.add(
                'body', 'name', 'Tag=%s already exists' % (new_tag_name,))
            self.request.errors.status = HTTPBadRequest.code
            return

        self.tags.rename_tag_name(tag_name, new_tag_name)
        tag = self.tags.get(new_tag_name)

        data = {
            'type' : "tag",
            'result' : tag.to_dict(),
        }
        return data

    @cornice_view(validators=(validators.url_tag_name_exists,))
    def delete(self):
        tag_name = self.request.matchdict['tag_name']
        tag = self.tags.get(tag_name)
        self.tags.remove(tag.name)

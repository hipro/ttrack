# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from persistent import Persistent
from persistent.mapping import PersistentMapping

from zope import interface, event
from repoze import folder

from ttrack import errors
from .interfaces import ITag, ITagAddedEvent, ITagWillBeRemovedEvent

@interface.implementer(ITagAddedEvent)
class TagAddedEvent(object):

    def __init__(self, tag):
        self.tag = tag

@interface.implementer(ITagWillBeRemovedEvent)
class TagWillBeRemovedEvent(object):

    def __init__(self, tag):
        self.tag = tag


class TagContainer(folder.Folder):

    def exists(self, tag_name):
        return tag_name in self

    def add(self, tag):
        # We convert the given tag name to lower case and use it as a
        # key in the container
        tag_name = tag.name.lower()
        super(TagContainer, self).add(tag_name, tag, send_events=False)
        event.notify(TagAddedEvent(tag))

    def add_by_name(self, tag_name):
        """Given a tag name, this method construct a new 'Tag' object and
        adds it to the container
        """
        tag = Tag(tag_name)
        self.add(tag)
        return tag

    def remove(self, tag_name):
        key = tag_name.strip().lower()
        tag = self.get(key)
        event.notify(TagWillBeRemovedEvent(tag))
        super(TagContainer, self).remove(key, send_events=False)

    def get(self, tag_name, *args):
        tag_name = tag_name.strip().lower()
        return super(TagContainer, self).get(tag_name, *args)

    def __contains__(self, tag_name):
        name = tag_name.strip().lower()
        return super(TagContainer, self).__contains__(name)

    def rename_tag_name(self, old_name, new_name):
        """PUT method shouldn't remove and add a tag for name change as that
        will cause all tag references in timelog entries to be
        lost. We need to do it in nicer way without disturbing the
        event subscribers
        """
        old_name = old_name.strip().lower()
        tag = super(TagContainer, self).get(old_name, None)
        assert tag is not None, "Tag (%s) does not exist" % (old_name,)
        tag.name = new_name.strip()
        # Don't propogate the IObjectWillBeRemovedEvent and
        # IObjectAddedEvent
        super(TagContainer, self).remove(old_name, send_events=False)
        # We have removed the old-tag, now check ensure that we don't
        # have conflict with the new-tag
        assert new_name not in self, "New tag (%s) already exists" % new_name
        new_name = new_name.strip().lower() # Key in lower-case
        super(TagContainer, self).add(new_name, tag, send_events=False)


@interface.implementer(ITag)
class Tag(Persistent):

    def __init__(self, name):
        super(Tag, self).__init__()
        self.name = name.strip()

    def to_dict(self):
        return {
            'name' : self.name,
        }

    @classmethod
    def from_dict(klass, data):
        tag = klass(name=data['name'])
        return tag

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    )

# cornice view error handlers

def body_tag_name_exists(request):
    tags = request.root['tags']
    tag_name = request.json.get("tag_name", None)
    if tag_name and tag_name not in tags:
        request.errors.add(
            'body', 'tag_name', "Tag='%s' does not exit" % tag_name)
        request.errors.status = HTTPBadRequest.code

def qs_tag_name_exists(request):
    tags = request.root['tags']
    tag_name = request.params.get("tag_name", None)
    if tag_name and tag_name not in tags:
        request.errors.add('querystring', 'tag_name',
                           "Tag='%s' does not exit" % tag_name)
        request.errors.status = HTTPBadRequest.code

def url_tag_name_exists(request):
    tags = request.root['tags']
    tag_name = request.matchdict["tag_name"]
    if tag_name not in tags:
        request.errors.add(
            'url', 'tag_name', "Tag='%s' does not exit" % tag_name)
        request.errors.status = HTTPNotFound.code

# pyramid view error handlers

def pv_url_tag_name_exists(request):
    tags = request.root['tags']
    tag_name = request.matchdict["tag_name"]
    if tag_name not in tags:
        raise HTTPNotFound("Tag='%s' does not exit" % tag_name)

def pv_qs_tag_name_exists(request):
    tags = request.root['tags']
    tag_name = request.params.get("tag_name", None)
    if tag_name and tag_name not in tags:
        raise HTTPBadRequest("Tag='%s' does not exit" % tag_name)

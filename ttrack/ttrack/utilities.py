# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import re

def extract_id(form_string):
    """
    In autocomplete widgets the string is formatted as "[id] name".
    From this string, using regex we will extract the 'id' part
    alone. If there is no 'id' we shall return the whole string. As
    per our regex pattern [id] part can be in anywhere in the given
    string. We return just the first match of it (of course without
    the square brackets around them).
    """
    st = form_string.strip()
    match = re.search("(?<=\[)(.*?)(?=\])", st)
    if match is None: return st
    return match.group()

class PaginatedTimelogContainer(object):

    def __init__(self, timelog, key_set, page_size):
        self.__timelog = timelog
        self.__key_set = key_set
        self.__page_size = page_size
        # compute the number of pages
        self.__page_count = len(key_set) / page_size
        if (page_size * self.__page_count) < len(key_set):
            self.__page_count += 1

    @property
    def page_count(self):
        return self.__page_count

    @property
    def size(self):
        return len(self.__key_set)

    def is_valid(self, page_number):
        if self.__page_count:
            if page_number > self.__page_count:
                return False
            else:
                return True
        else:
            if page_number != 1:
                return False
        return True

    def get(self, page_number):
        start_index = -(page_number * self.__page_size)
        end_index = -((page_number - 1) * self.__page_size)
        if end_index == 0:
            keys = self.__key_set[start_index:]
        else:
            keys = self.__key_set[start_index:end_index]

        entries = self.__timelog.get_ite_entries(keys[0], keys[-1])
        return reversed(entries)


class PaginatedContainer(object):
    def __init__(self, container, page_size):
        self.__container = container
        self.__page_size = page_size
        # compute the number of pages
        self.__page_count = len(self.__container) / page_size
        if (page_size * self.__page_count) < len(container):
            self.__page_count += 1

    @property
    def page_count(self):
        return self.__page_count

    @property
    def size(self):
        return len(self.__container)

    def is_valid(self, page_number):
        if self.__page_count:
            if page_number > self.__page_count:
                return False
            else:
                return True
        else:
            if page_number != 1:
                return False
        return True

    def get(self, page_number):
        start_index = -(page_number * self.__page_size)
        end_index = -((page_number - 1) * self.__page_size)
        if end_index == 0:
            entries = reversed(self.__container[start_index:])
        else:
            entries = reversed(self.__container[start_index:end_index])
        return entries


def notify(event, context=None, registry=None):
    event.root = None
    if context:
        event.root = find_root(context)
    if not registry:
        registry = get_current_registry()
    registry.notify(event)

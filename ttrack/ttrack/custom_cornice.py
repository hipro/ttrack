# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import HTTPBadRequest

def validate_colander_schema(schema, request):
    """Validates that the request is conform to the given schema"""

    from cornice.util import to_list, extract_request_data
    from cornice.schemas import CorniceSchema
    from colander import Invalid, Sequence, drop, null

    unknowns_options = {
        "path" : "ignore",
        "header" : "ignore",
        "body" : "ignore",
        "querystring" : "ignore",
        }
    try:
        options = schema._c_schema.cornice_unknowns_options()
        unknowns_options.update(options)
    except AttributeError:
        pass

    def _validate_fields(location, data):
        attrs = schema.get_attributes(location=location,
                                      request=request)
        error_on_unknowns = False
        if unknowns_options[location] == 'error':
            error_on_unknowns = True
            data = data.copy()

        for attr in attrs:
            if attr.required and attr.name not in data and attr.default == null:
                # missing
                request.errors.add(location, attr.name,
                                   "%s is missing" % attr.name)
            else:
                try:
                    if not attr.name in data:
                        if attr.default != null:
                            deserialized = attr.deserialize(attr.serialize())
                        else:
                            deserialized = attr.deserialize()
                    else:
                        if (location == 'querystring' and
                                isinstance(attr.typ, Sequence)):
                            serialized = data.getall(attr.name)
                            if error_on_unknowns:
                                for i in xrange(len(serialized)):
                                    data.pop(attr.name)
                        else:
                            serialized = data[attr.name]
                            if error_on_unknowns:
                                data.pop(attr.name)
                        deserialized = attr.deserialize(serialized)
                except Invalid as e:
                    # the struct is invalid
                    try:
                        request.errors.add(location, attr.name,
                                           e.asdict()[attr.name])
                    except KeyError:
                        for k, v in e.asdict().items():
                            if k.startswith(attr.name):
                                request.errors.add(location, k, v)
                else:
                    if deserialized is not drop:
                        request.validated[attr.name] = deserialized

        if error_on_unknowns:
            for k in dict(data):
                request.errors.add(location, k, "Unknown parameter")
                request.errors.status = HTTPBadRequest.code

    qs, headers, body, path = extract_request_data(request)

    _validate_fields('path', path)
    _validate_fields('header', headers)
    _validate_fields('body', body)
    _validate_fields('querystring', qs)

def install():
    import cornice.schemas
    import cornice.service
    cornice.service.validate_colander_schema = validate_colander_schema

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from zope import component

from .interfaces import IApplicationCreatedEvent
from ttrack.users import User

@component.adapter(IApplicationCreatedEvent)
def populate_db(event):
    # At this point all the module container will be added. Now,
    # populate initial data
    # Add default set of users
    users = event.app['users']
    user = User("admin", "Administrator", "admin", "admin@admin.com")
    users.add(user)

def register_zca_subscribers():
    component.provideHandler(populate_db)

register_zca_subscribers()

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from persistent import Persistent
from persistent.list import PersistentList
from BTrees.OOBTree import OOTreeSet

from zope import interface, event
from repoze import folder

from pyramid.security import Allow, ALL_PERMISSIONS

from ttrack import errors
from .interfaces import (
    IProject,
    IProjectAddedEvent,
    IProjectWillBeRemovedEvent,
    IActivity,
    IActivityAddedEvent,
    IActivityWillBeRemovedEvent,
    IProjectRole,
    IProjectRoleWillBeRemovedEvent
)

PROJECT_PERMISSIONS = {
    'Add/Edit/View activities' : ('activities.manage', 'activities.view'),
    'View activities' : ('activities.view',),
    'Add/Remove/View members' : ('members.manage', 'members.view'),
    'View members' : ('members.view',),
    'Add/Edit/View project roles' : (
        'project.roles.manage', 'project.roles.view'),
    'View project roles' : ('project.roles.view',),
    'Add/Edit/View personal timelogs' : ('timesheet.manage', 'timesheet.view'),
    'Add/Edit/View others timelogs for common projects' : (
        'timesheet.manage.others', 'timesheet.view'),
    'View timelogs' : ('timesheet.view',),
}

def _get_all_permissions():
    perm = set()
    for v in PROJECT_PERMISSIONS.values():
        perm.update(v)
    return tuple(perm)

def get_default_member_permission_keys():
    return (
        'Add/Edit/View personal timelogs',
        'View members',
        'View activities'
    )

def get_project_manager_permission_keys():
    return (
        'Add/Edit/View activities',
        'Add/Remove/View members',
        'Add/Edit/View project roles',
        'Add/Edit/View personal timelogs',
    )

@interface.implementer(IProjectAddedEvent)
class ProjectAddedEvent(object):

    def __init__(self, project):
        self.project = project

@interface.implementer(IProjectWillBeRemovedEvent)
class ProjectWillBeRemovedEvent(object):

    def __init__(self, project):
        self.project = project

@interface.implementer(IActivityAddedEvent)
class ActivityAddedEvent(object):

    def __init__(self, project, activity):
        self.project = project
        self.activity = activity

@interface.implementer(IActivityWillBeRemovedEvent)
class ActivityWillBeRemovedEvent(object):

    def __init__(self, project, activity):
        self.project = project
        self.activity = activity

@interface.implementer(IProjectRoleWillBeRemovedEvent)
class ProjectRoleWillBeRemovedEvent(object):

    def __init__(self, project, role):
        self.project = project
        self.role = role

class ProjectContainer(folder.Folder):

    def exists(self, project_id):
        return project_id in self

    def add(self, project):
        super(ProjectContainer, self).add(
            project.project_id, project, send_events=False)

        event.notify(ProjectAddedEvent(project))

    def is_id_unique(self, project_id):
        return project_id not in self

    def get(self, project_id, *args):
        project_id = project_id.strip().lower()
        return super(ProjectContainer, self).get(project_id, *args)

    def __contains__(self, project_id):
        project_id = project_id.strip().lower()
        return super(ProjectContainer, self).__contains__(project_id)

    def remove(self, project_id):
        project_id = project_id.strip().lower()
        project = self.get(project_id)
        event.notify(ProjectWillBeRemovedEvent(project))
        return super(ProjectContainer, self).remove(
            project_id, send_events=False)

    def rename_project_id(self, old_id, new_id):
        """This method will be exclusively used by the evolution script to
        migrate project ids which contains upper-case letters in them.
        """
        proj = super(ProjectContainer, self).get(old_id, None)
        assert proj is not None, "Old project id (%s) does not exist" % old_id
        assert new_id not in self, "New project id (%s) already exists" % new_id
        project_id = new_id.strip().lower()
        proj.project_id = project_id
        # Don't propogate the IObjectWillBeRemovedEvent and
        # IObjectAddedEvent
        super(ProjectContainer, self).remove(old_id, send_events=False)
        super(ProjectContainer, self).add(project_id, proj, send_events=False)


@interface.implementer(IProject)
class Project(Persistent):

    def __init__(self, project_id, name=u""):
        super(Project, self).__init__()
        self.project_id = project_id.strip().lower()
        self.name = name.strip()
        self.client = None
        # Sub containers should be made location aware. So, we are
        # passing 'self' to the sub container to set its parent
        self.activities = ActivityContainer(self)
        self.members = MemberContainer(self)
        self.roles = ProjectRoleContainer(self)

    def __acl__(self):
        # Make the ACL dynamic so that we don't need to set the ACL
        # explicitly by event subscriber mechanism everytime when
        # project role changes
        return self.roles.get_acl()

    def fix_acl(self):
        """This method will be exclusively used by migration script to
        convert self.__acl__ from PersistentList() to callable
        """
        if isinstance(self.__acl__, PersistentList):
            log.info("Project (%s): Fixing acl and roles", self.project_id)
            if not hasattr(self, 'roles'):
                self.roles = ProjectRoleContainer(self)
            del self.__acl__

    def set_client(self, client):
        self.client = client
        client.add_project(self)

    def to_dict(self):
        client_id = None
        if self.client:
            client_id = self.client.client_id
        project = {
            'project_id' : self.project_id,
            'name' : self.name,
            'client_id' : client_id,
            'activities' : [a.to_dict() for a in self.activities.values()],
            'members' : [u.to_dict() for u in self.members.values()],
            }
        return project

    @classmethod
    def from_dict(klass, data):
        project = klass(data['project_id'], data['name'])
        project.set_client(data['client'])
        if 'admin_user' in data:
            member = Member(data['admin_user'])
            member.role = project.roles.get('admin')
            project.add_member(member)
        return project

    def get_activity(self, activity_name, *args):
        return self.activities.get(activity_name, *args)

    def add_activity(self, activity_name):
        return self.activities.add_by_name(activity_name)

    def add_member(self, member):
        self.members.add(member)
        member.user.add_project(self)

    def remove_member(self, member):
        user = member.user
        self.members.remove(member)
        user.remove_project(self)

    def get_permissions(self, user=None):
        # Get the user object and return the permissions of that user
        # on this project
        if not user or user.userid not in self.members:
            return []
        member = self.members.get(user.userid)
        return member.get_permissions()

    def get_group(self, user):
        # Get the role_id of the given user
        if user.userid not in self.members:
            return []
        member = self.members.get(user.userid)
        role_id = member.get_role_id()
        return ['%s:%s' % (self.project_id, role_id),]

class ProjectRoleContainer(folder.Folder):

    def __init__(self, parent=None):
        super(ProjectRoleContainer, self).__init__()
        # Making location aware
        self.__parent__ = parent
        self.__name__ = "roles"
        self._add_default_roles()

    def get(self, role_id, *args):
        role_id = role_id.strip().lower()
        return super(ProjectRoleContainer, self).get(role_id, *args)

    def __contains__(self, role_id):
        role_id = role_id.strip().lower()
        return super(ProjectRoleContainer, self).__contains__(role_id)

    def _add_default_roles(self):
        project_id = self.__parent__.project_id
        self.add(ProjectRole('admin', 'Project Admin'))
        r = ProjectRole('manager', 'Project Manager')
        for p in get_project_manager_permission_keys():
            r.add_permission(p)
        self.add(r)
        r = ProjectRole('member', 'Member')
        for p in get_default_member_permission_keys():
            r.add_permission(p)
        self.add(r)

    def add(self, role):
        # We don't need to raise an event explictly in order to set
        # the ACL, the Project object's ACL is dynamic
        super(ProjectRoleContainer, self).add(
            role.role_id, role, send_events=False)

    def remove(self, role_id):
        role_id = role_id.strip().lower()
        if role_id == 'admin':
            raise errors.BadParameter('Admin project role cannot be deleted')
        role = self.get(role_id)
        # We raise this event so that project member in this role will
        # be migrated to None (i.e they will become default members)
        event.notify(ProjectRoleWillBeRemovedEvent(self.__parent__, role))
        super(ProjectRoleContainer, self).remove(role_id, send_events=False)

    def get_acl(self):
        proj = self.__parent__
        acl = [
            (Allow, 'admin', ALL_PERMISSIONS),
        ]
        # Add all permissions to 'p:admin' role
        rid = '%s:admin' % (proj.project_id,)
        acl.append((Allow, rid, _get_all_permissions()))

        # Add permissions of other roles
        for r in self.values():
            # We have already added all permissions to 'p:admin' so
            # skip, if the role_id is 'admin'
            if r.role_id == 'admin':
                continue
            for p in r.permissions:
                acl.append(
                    (
                        Allow,
                        '%s:%s' % (proj.project_id, r.role_id),
                        PROJECT_PERMISSIONS[p]
                    )
                )
        return acl

@interface.implementer(IProjectRole)
class ProjectRole(Persistent):

    def __init__(self, role_id, name):
        super(ProjectRole, self).__init__()
        self.role_id = role_id.strip().lower()
        self.name = name.strip()
        self.permissions = OOTreeSet()

    def to_dict(self):
        return {
            'role_id' : self.role_id,
            'name' : self.name,
            'permissions' : [p for p in self.permissions],
        }

    @classmethod
    def from_dict(klass, data):
        role = klass(role_id=data['role_id'], name=data['name'])
        if 'permissions' in data:
            for permission in data['permissions']:
                role.permissions.add(permission)
        return role

    def add_permission(self, permission):
        if permission not in PROJECT_PERMISSIONS:
            raise errors.BadParameter("Invalid permission='%s'" % (permission,))
        self.permissions.add(permission)

    def remove_permission(self, permission):
        self.permissions.remove(permission)

    def get_permissions(self):
        if self.role_id == 'admin':
            return _get_all_permissions()
        perm = set()
        for p in self.permissions:
            perm.update(PROJECT_PERMISSIONS[p])
        return tuple(perm)

class MemberContainer(folder.Folder):

    def __init__(self, parent=None):
        super(MemberContainer, self).__init__()
        # Making location aware
        self.__parent__ = parent
        self.__name__ = "members"

    def get(self, userid, *args):
        userid = userid.strip().lower()
        return super(MemberContainer, self).get(userid, *args)

    def __contains__(self, userid):
        userid = userid.strip().lower()
        return super(MemberContainer, self).__contains__(userid)

    def add(self, member):
        user = member.user
        super(MemberContainer, self).add(user.userid, member, send_events=False)

    def remove(self, member):
        user = member.user
        super(MemberContainer, self).remove(user.userid, send_events=False)

    def get_role_id(self, userid):
        member = self.get(userid)
        if member and member.role:
            return member.role.role_id
        return ''

    def find_and_replace_roles(self, old_role, new_role):
        for member in self.values():
            if member.role == old_role:
                member.role = new_role

    def convert_user_to_member_object(self, user, project_role):
        """This method will be exclusively used by migration script to
        convert all old User object in this container to Member object
        """
        super(MemberContainer, self).remove(user.userid, send_events=False)
        mem = Member(user)
        mem.role = project_role
        self.add(mem)



class Member(Persistent):

    def __init__(self, user):
        super(Member, self).__init__()
        self.user = user
        self.role = None

    def to_dict(self):
        data = {
            'userid' : self.user.userid,
            'real_name' : self.user.real_name,
        }
        role_id = ''
        role_name = ''
        if self.role:
            role_id = self.role.role_id
            role_name = self.role.name
        data['role_id'] = role_id
        data['role_name'] = role_name
        return data

    def get_permissions(self):
        if self.role is None:
            return []
        if self.role.role_id == 'admin':
            return _get_all_permissions()
        return self.role.get_permissions()

    def get_role_id(self):
        if self.role is None:
            return ''
        return self.role.role_id

class ActivityContainer(folder.Folder):

    def __init__(self, parent=None):
        super(ActivityContainer, self).__init__()
        # Making location aware
        self.__parent__ = parent
        self.__name__ = "activities"

    def exists(self, activity_name):
        name = activity_name.strip().lower()
        return name in self

    def add(self, activity):
        # Just like tag, here too we convert the given activity name
        # to lower case and use it as a key in the container
        name = activity.name.lower()
        super(ActivityContainer, self).add(name, activity, send_events=False)
        event.notify(ActivityAddedEvent(self.__parent__, activity))

    def add_by_name(self, activity_name):
        """Given the activity_name, this method constructs a new activity and
        adds it to the container
        """
        act = Activity(activity_name)
        self.add(act)
        return act

    def remove(self, activity_name):
        name = activity_name.strip().lower()
        act = self.get(name)
        event.notify(ActivityWillBeRemovedEvent(self.__parent__, act))
        super(ActivityContainer, self).remove(name, send_events=False)

    def get(self, activity_name, *args):
        name = activity_name.strip().lower()
        return super(ActivityContainer, self).get(name, *args)

    def __contains__(self, activity_name):
        name = activity_name.strip().lower()
        return super(ActivityContainer, self).__contains__(name)

    def rename_activity_name(self, old_name, new_name):
        """PUT method shouldn't remove and add a activity for name change as
        that will cause all timelog entries of that activity to be
        lost. We need to do it in nicer way without disturbing the
        event subscribers
        """
        old_name = old_name.strip().lower()
        act = super(ActivityContainer, self).get(old_name, None)
        assert act is not None, "Activity (%s) does not exist" % (old_name,)
        act.name = new_name.strip()
        # Don't propogate the IObjectWillBeRemovedEvent and
        # IObjectAddedEvent
        super(ActivityContainer, self).remove(old_name, send_events=False)
        # We have removed the old-act, now check ensure that we don't
        # have conflict with the new-act
        assert new_name not in self, "New Activity (%s) already exists" % (
            new_name,)
        new_name = new_name.strip().lower() # Key in lower-case
        super(ActivityContainer, self).add(new_name, act, send_events=False)


@interface.implementer(IActivity)
class Activity(Persistent):

    def __init__(self, name):
        super(Activity, self).__init__()
        self.name = name.strip()

    def to_dict(self):
        return {
            'name': self.name,
        }

    @classmethod
    def from_dict(klass, data):
        act = klass(name=data['name'])
        return act

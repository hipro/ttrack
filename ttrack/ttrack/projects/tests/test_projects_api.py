# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    )
from pyramid.authorization import ACLAuthorizationPolicy
from zope import component
import zope.component.testing as zc_testing
from repoze.folder.interfaces import (
    IObjectWillBeRemovedEvent,
)
from ttrack.projects.interfaces import IProject
from ttrack import projects
from ttrack import clients

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.users.models import (
    UserContainer,
    User
)

from ttrack.projects.models import (
    ProjectContainer,
    Project,
)

from ttrack.clients.models import (
    ClientContainer,
    Client,
)

USER_DATA = (
    {
        'userid' : 'john',
        'real_name' : 'John doe',
        'password' : 'john',
        'email_id' : 'john@user.com',
        'role_id' : '',
    },

    {
        'userid' : 'admin',
        'real_name' : 'Jane Kirkland',
        'password' : 'jane',
        'email_id' : 'jane@user.com',
        'role_id' : '',
    },
)

CLIENT_DATA = (
    {
        'client_id' : 'c1',
        'name': 'Client 1',
        'address': 'c1 Address',
        },
    {
        'client_id' : 'c2',
        'name' : 'Client 2',
        'address' : 'c2 Address',
        },
    )

PROJECT_DATA = (
    {
        'project_id' : 'p1',
        'client' : 'c1',
        'name': 'Project One',
        'userid' : 'john',
    },
    {
        'project_id' : 'p2',
        'client' : 'c1',
        'name' : 'Project Two',
        'userid' : 'admin',
    },
    {
        'project_id' : 'p3',
        'client' : 'c2',
        'name' : 'Project Three',
        'userid' : 'admin',
    },
    {
        'project_id' : 'pfour',
        'client' : 'c2',
        'name' : 'Project Four',
        'userid' : 'john',
    }
)


class TestRoot(dict):
    __acl__ = ((Allow, Authenticated, "projects.view"),
               (Allow, "sg:admin", ALL_PERMISSIONS))
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(udata, cdata, pdata):
    root = TestRoot()
    root['users'] = UserContainer()
    root['clients'] = ClientContainer()
    root['projects'] = ProjectContainer()

    for u in udata:
        user = User.from_dict(u)
        root['users'].add(user)

    for c in cdata:
        client = Client.from_dict(c)
        root['clients'].add(client)

    for p in pdata:
        pd = {}
        pd.update(p)
        pd['client'] = root['clients'].get(p['client'])
        pd['admin_user'] = root['users'].get(p['userid'])
        project = Project.from_dict(pd)
        root['projects'].add(project)

    def root_factory(request):
        return root
    return root_factory

def project_in_list(data, project_list):
    project_id = data
    if type(data) is dict:
        project_id = data['project_id']
    if type(project_id) is not str:
        raise TypeError("Unable to determine project-id. Bad 'data'.")
    for project in project_list:
        if project_id == project['project_id']:
            return True
    return False

class ProjectAPITests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/projects'
        self.clients_api_url = '/api/v1/clients'
        zc_testing.setUp()
        registry = component.getGlobalSiteManager()
        self.config = testing.setUp(registry=registry)
        self.config.setup_registry(
            root_factory=build_root_factory(
                USER_DATA, CLIENT_DATA, PROJECT_DATA))
        projects.register_zca_subscribers()
        clients.register_zca_subscribers()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.scan("ttrack.projects.api")
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()
        zc_testing.tearDown()

    def test_get_all_projects(self):
        resp = self.app.get(self.api_url, status=HTTPOk.code)
        projects = resp.json['result']
        self.assertEqual(len(projects), len(PROJECT_DATA))
        for d in PROJECT_DATA:
            self.assertTrue(project_in_list(d, projects))

    def test_get_existing_project(self):
        resp = self.app.get(self.api_url + '/p1.json', status=HTTPOk.code)
        project = resp.json['result']
        self.assertEqual('p1', project['project_id'])

    def test_get_existing_project_case_insensitive(self):
        resp = self.app.get(self.api_url + '/pFour.json', status=HTTPOk.code)
        project = resp.json['result']
        self.assertEqual(project['project_id'], 'pfour')

    def test_get_non_existant_project(self):
        self.app.get(self.api_url + '/unknown.json', status=HTTPNotFound.code)

    def test_add_new(self):
        new_project = {
            'project_id' : 'new',
            'client_id' : 'c1',
            'name' : 'New Project',
            'userid' : 'john',
            }
        resp = self.app.post_json(self.api_url, new_project, status=HTTPOk.code)
        project = resp.json['result']
        self.assertEqual(new_project['project_id'], project['project_id'])

        resp = self.app.get(self.api_url + '/new.json', status=HTTPOk.code)
        project = resp.json['result']
        self.assertEqual(new_project['project_id'], project['project_id'])

        # Check whether the admin_user is added as member
        member = project['members'][0]
        self.assertEqual(member['userid'], 'john')

    def test_add_fail_on_non_unique_project_id(self):
        new_project = {
            'project_id' : 'p1',
            'client_id' : 'c1',
            'name' : 'New Project',
            'userid' : 'john',
            }
        self.app.post_json(self.api_url, new_project, status=HTTPBadRequest.code)

    def test_add_fail_on_non_unique_project_id_case_insensitive(self):
        new_project = {
            'project_id' : 'PFOUR',
            'client_id' : 'c1',
            'name' : 'New Project',
            'userid' : 'john',
            }
        self.app.post_json(self.api_url, new_project, status=HTTPBadRequest.code)

    def test_add_fail_on_bad_schema(self):
        new_project = {
            'project_id' : 'new',
            }
        self.app.post_json(self.api_url, new_project, status=HTTPBadRequest.code)

    def test_add_fail_on_non_existant_client_id(self):
        new_project = {
            'project_id' : 'new',
            'client_id' : 'unknown',
            'name' : 'New Project',
            'userid' : 'john',
            }
        self.app.post_json(self.api_url, new_project, status=HTTPBadRequest.code)

    def test_add_fail_on_non_existant_userid(self):
        new_project = {
            'project_id' : 'new',
            'client_id' : 'c2',
            'name' : 'New Project',
            'userid' : 'smith',
            }
        self.app.post_json(self.api_url, new_project, status=HTTPBadRequest.code)

    def test_add_links_to_client(self):
        new_project = {
            'project_id' : 'new',
            'client_id' : 'c2',
            'name' : 'New Project',
            'userid' : 'john',
            }
        self.app.post_json(self.api_url, new_project, status=HTTPOk.code)
        resp = self.app.get(
            self.clients_api_url + '/c2.json', status=HTTPOk.code)
        client = resp.json['result']
        self.assertTrue(project_in_list('new', client['projects']))

    def test_put_change_project_name(self):
        project = {
            'project_id' : 'p1',
            'name' : 'Updated name',
            }
        self.app.put_json(self.api_url + '/p1.json', project, status=HTTPOk.code)
        resp = self.app.get(self.api_url + '/p1.json', status=HTTPOk.code)
        upd_project = resp.json['result']
        self.assertEqual(project['name'], upd_project['name'])

    def test_put_do_not_allow_client_change(self):
        project = {
            'project_id' : 'p1',
            'client_id' : 'c2',
            }
        self.app.put_json(
            self.api_url + '/p1.json', project, status=HTTPOk.code)
        resp = self.app.get(
            self.clients_api_url + '/c1.json', status=HTTPOk.code)
        self.assertTrue(project_in_list('p1', resp.json['result']['projects']))
        resp = self.app.get(
            self.clients_api_url + '/c2.json', status=HTTPOk.code)
        self.assertFalse('p1' in resp.json['result']['projects'])

    def test_put_fail_on_project_id_change(self):
        project = {
            'project_id' : 'new',
            }
        self.app.put_json(self.api_url + '/p1.json', project,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_different_case_sensitive_project_id_1(self):
        project = {
            'project_id' : 'pfour',
            'name' : 'Updated name',
            }
        self.app.put_json(self.api_url + '/pFour.json', project,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_different_case_sensitive_project_id_2(self):
        project = {
            'project_id' : 'PFour',
            'name' : 'Updated name',
            }
        self.app.put_json(self.api_url + '/pfour.json', project,
                          status=HTTPBadRequest.code)

    def test_put_fail_on_non_existant_project_id(self):
        project = {
            'project_id' : 'other',
            }
        self.app.put_json(self.api_url + '/other.json', project,
                          status=HTTPBadRequest.code)

    def test_delete(self):
        self.app.delete(self.api_url + '/p1.json', status=HTTPOk.code)
        self.app.get(self.api_url + '/p1.json', status=HTTPNotFound.code)

    def test_delete_non_existant_project(self):
        self.app.delete(
            self.api_url + '/other.json', status=HTTPBadRequest.code)

    def test_delete_case_insensitive(self):
        self.app.delete(self.api_url + '/pFouR.json', status=HTTPOk.code)
        self.app.get(self.api_url + '/pfour.json', status=HTTPNotFound.code)

    def test_delete_unlinks_client(self):
        resp = self.app.get(
            self.clients_api_url + '/c1.json', status=HTTPOk.code)
        self.assertTrue(project_in_list('p1', resp.json['result']['projects']))
        self.app.delete(self.api_url + '/p1.json', status=HTTPOk.code)
        resp = self.app.get(
            self.clients_api_url + '/c1.json', status=HTTPOk.code)
        self.assertFalse('p1' in resp.json['result']['projects'])

class ProjectAPINonAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/projects'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(userid='john', groupids=[])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url + '/p1.json', status=HTTPOk.code)

    def test_deny_add(self):
        project = {
            'project_id' : 'new',
            'name' : 'New Project',
            }
        self.app.post_json(self.api_url, project, status=HTTPForbidden.code)

    def test_deny_update(self):
        project = {
            'project_id' : 'p1',
            'name' : 'New Project',
            }
        self.app.put_json(self.api_url + '/p1.json', project,
                          status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/p1.json', status=HTTPForbidden.code)


class ProjectAPIAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/projects'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid='admin', groupids=['sg:admin'])
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url, status=HTTPOk.code)

    def test_allow_get(self):
        self.app.get(self.api_url + '/p1.json', status=HTTPOk.code)

    def test_allow_add(self):
        project = {
            'project_id' : 'new',
            'name' : 'New Project',
            'client_id' : 'c1',
            'userid' : 'john',
            }
        self.app.post_json(self.api_url, project, status=HTTPOk.code)

    def test_allow_update(self):
        project = {
            'project_id' : 'p1',
            'name' : 'New Project',
            }
        self.app.put_json(
            self.api_url + '/p1.json', project, status=HTTPOk.code)

    def test_allow_delete(self):
        self.app.delete(self.api_url + '/p1.json', status=HTTPOk.code)


class ProjectAPIUnAuthenticatedPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url = '/api/v1/projects'
        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy()
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_deny_get_collection(self):
        self.app.get(self.api_url, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url + '/p1.json', status=HTTPForbidden.code)

    def test_deny_add(self):
        project = {
            'project_id' : 'new',
            'name' : 'New Project',
            }
        self.app.post_json(self.api_url, project, status=HTTPForbidden.code)

    def test_deny_update(self):
        project = {
            'project_id' : 'p1',
            'name' : 'New Project',
            }
        self.app.put_json(
            self.api_url + '/p1.json', project, status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url + '/p1.json', status=HTTPForbidden.code)

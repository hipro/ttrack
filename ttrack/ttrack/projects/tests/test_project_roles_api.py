# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden,
    HTTPMethodNotAllowed,
    )
from pyramid import testing
from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
    DENY_ALL,
    )
from pyramid.authorization import ACLAuthorizationPolicy

from webtest import TestApp

from cornice.tests.support import CatchErrors

from ttrack.projects.models import (
    ProjectContainer,
    Project,
    Member,
    )

from ttrack.users.models import User, UserContainer, find_group
from ttrack.clients.models import (
    ClientContainer,
    Client,
    )

USER_DATA = (
    {"userid" : "admin",
     "real_name" : "Administrator",
     "email_id" : "admin@admin.org",
     "password" : "pass"},
    {"userid" : "john",
     "real_name" : "John Doe",
     "email_id" : "john@doe.org",
     "password" : "john"},
    {"userid" : "smith",
     "real_name" : "Jane Smith",
     "email_id" : "jane@smith.org",
     "password" : "jane"},
    {"userid" : "doe",
     "real_name" : "John Doe",
     "email_id" : "doe@smith.org",
     "password" : "doe"}
)

CLIENT_DATA = (
    {
        'client_id' : 'c1',
        'name': 'Client 1',
        'address': 'c1 Address',
        },
    {
        'client_id' : 'c2',
        'name' : 'Client 2',
        'address' : 'c2 Address',
        },
    {
        'client_id' : 'c3',
        'name' : 'Client 3',
        'address' : 'c3 Address',
        },
    )

PROJECT_DATA = (
    {
        'project_id' : 'p1',
        'client' : 'c1',
        'name': 'Project One',
        'userid' : 'john',
        'members' : ['smith', 'doe']
    },
    {
        'project_id' : 'p2',
        'client' : 'c1',
        'name' : 'Project Two',
        'userid' : 'admin',
        'members' : ['doe', 'smith']
    },
)

class TestRoot(dict):
    __acl__ = ((Allow, Authenticated, "view"),
               (Allow, "sg:admin", ALL_PERMISSIONS),
               DENY_ALL)
    def __init__(self, *args, **kw):
        super(TestRoot, self).__init__(*args, **kw)

def build_root_factory(udata, cdata, pdata):
    root = TestRoot()
    root['users'] = UserContainer()
    root['clients'] = ClientContainer()
    root['projects'] = ProjectContainer()

    for u in udata:
        user = User.from_dict(u)
        root['users'].add(user)

    for c in cdata:
        client = Client.from_dict(c)
        root['clients'].add(client)

    for p in pdata:
        pd = {}
        pd.update(p)
        pd['client'] = root['clients'].get(p['client'])
        pd['admin_user'] = root['users'].get(p['userid'])
        project = Project.from_dict(pd)
        for m in p['members']:
            mem = Member(root['users'].get(m))
            mem.role = project.roles.get('member')
            project.add_member(mem)
        root['projects'].add(project)

    def root_factory(request):
        return root
    return root_factory

class ProjectRolesAPITests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = "/api/v1/project/roles/p1"
        self.api_url_p2 = "/api/v1/project/roles/p2"
        self.api_url_not_exist = "/api/v1/project/roles/p3"

        self.config = testing.setUp()
        self.config.include("cornice")
        self.config.testing_securitypolicy(
            userid="admin", groupids=("sg:admin",), permissive=True)
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(
            build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA))
        self.config.scan("ttrack.projects.api")
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_get_all_roles_for_existent_project(self):
        resp = self.app.get(self.api_url_p1, status=HTTPOk.code)
        roles = resp.json['result']
        # By default, 3 roles will be returned for any new project
        # 'admin', 'manager' and 'member'
        self.assertEqual(len(roles), 3)

    def test_get_all_roles_for_non_existent_project(self):
        self.app.get(self.api_url_not_exist, status=HTTPNotFound.code)

    def test_get_default_role_1(self):
        resp = self.app.get(self.api_url_p1 + "/admin.json", status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual('admin', role['role_id'])

    def test_get_default_role_2(self):
        resp = self.app.get(
            self.api_url_p2 + "/manager.json", status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual('manager', role['role_id'])

    def test_get_default_role_3(self):
        resp = self.app.get(
            self.api_url_p1 + "/member.json", status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual('member', role['role_id'])

    def test_get_role_case_insensitive(self):
        resp = self.app.get(
            self.api_url_p1 + "/Member.json", status=HTTPOk.code)
        role = resp.json['result']
        self.assertEqual('member', role['role_id'])

    def test_add_new_role_for_existent_project(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p2, role, status=HTTPOk.code)
        resp = self.app.get(
            self.api_url_p2 + "/design.json", status=HTTPOk.code)
        log.debug(resp)
        new_role = resp.json['result']
        self.assertEqual(new_role['role_id'], role['role_id'])
        self.assertEqual(new_role['name'], role['name'])
        self.assertEqual(len(new_role['permissions']), len(role['permissions']))

        resp = self.app.get(self.api_url_p2, status=HTTPOk.code)
        roles = resp.json['result']
        self.assertEqual(len(roles), 4)

    def test_add_new_role_fail_for_duplicate_role_id(self):
        role = {
            'role_id' : 'manager',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p2, role, status=HTTPBadRequest.code)

    def test_add_new_role_fail_for_duplicate_role_id_case_insensitive(self):
        role = {
            'role_id' : 'AdMIN',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p2, role, status=HTTPBadRequest.code)

    def test_add_new_role_fail_on_missing_permissions(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
        }
        self.app.post_json(self.api_url_p2, role, status=HTTPBadRequest.code)

    def test_add_new_role_fail_on_empty_permissions(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
            'permissions' : []
        }
        self.app.post_json(self.api_url_p1, role, status=HTTPBadRequest.code)

    def test_add_new_role_fail_on_invalid_permissions(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',
                             'Add/Edit/View timelogs for me',]
        }
        self.app.post_json(self.api_url_p1, role, status=HTTPBadRequest.code)

    def test_add_new_role_fail_on_missing_name(self):
        role = {
            'role_id' : 'design',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p1, role, status=HTTPBadRequest.code)

    def test_add_new_role_fail_on_empty_name(self):
        role = {
            'role_id' : 'design',
            'name' : '',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p1, role, status=HTTPBadRequest.code)

    def test_put_fail_on_non_existent_project(self):
        role = {
            'role_id' : 'manager',
            'name' : 'My new manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_not_exist + '/manager.json', role,
            status=HTTPBadRequest.code)

    def test_put_fail_on_role_id_change(self):
        role = {
            'role_id' : 'manager2',
            'name' : 'My new manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p1 + '/manager.json', role, status=HTTPBadRequest.code)

    def test_put_fail_on_admin_role(self):
        role = {
            'role_id' : 'admin',
            'name' : 'My new admin',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p2 + '/admin.json', role, status=HTTPBadRequest.code)

    def test_put_fail_on_non_existent_role(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p1 + '/design.json', role, status=HTTPBadRequest.code)

    def test_put_on_modifiable_default_role_1(self):
        resp = self.app.get(self.api_url_p1 + '/manager.json',
                            status=HTTPOk.code)
        old_role = resp.json['result']
        self.assertEqual(len(old_role['permissions']), 4)

        role = {
            'role_id' : 'manager',
            'name' : 'My new Manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p1 + '/manager.json', role, status=HTTPOk.code)
        resp = self.app.get(self.api_url_p1 + '/manager.json',
                            status=HTTPOk.code)
        new_role = resp.json['result']
        self.assertEqual(len(new_role['permissions']), 1)

    def test_put_on_modifiable_default_role_2(self):
        resp = self.app.get(self.api_url_p1 + '/member.json',
                            status=HTTPOk.code)
        old_role = resp.json['result']
        self.assertEqual(len(old_role['permissions']), 3)

        role = {
            'role_id' : 'member',
            'name' : 'My new Member',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p1 + '/member.json', role, status=HTTPOk.code)
        resp = self.app.get(self.api_url_p1 + '/member.json',
                            status=HTTPOk.code)
        new_role = resp.json['result']
        self.assertEqual(len(new_role['permissions']), 1)

    def test_put_on_case_insensitive_url_role_id(self):
        role = {
            'role_id' : 'manager',
            'name' : 'My new Manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p1 + '/ManaGER.json', role, status=HTTPOk.code)

    def test_put_on_case_insensitive_body_role_id(self):
        role = {
            'role_id' : 'MANAGER',
            'name' : 'My new Manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p1 + '/manager.json', role, status=HTTPOk.code)

    def test_delete_fail_on_default_admin_role_id(self):
        self.app.delete(self.api_url_p1 + "/admin.json",
                        status=HTTPBadRequest.code)

    def test_delete_fail_on_non_existent_project(self):
        self.app.delete(self.api_url_not_exist + "/manager.json",
                        status=HTTPBadRequest.code)

    def test_delete_fail_on_non_existent_role_id(self):
        self.app.delete(self.api_url_p2 + "/manager2.json",
                        status=HTTPBadRequest.code)

    def test_delete_existent_role_id(self):
        self.app.delete(self.api_url_p2 + "/manager.json", status=HTTPOk.code)
        resp = self.app.get(self.api_url_p2, status=HTTPOk.code)
        roles = resp.json['result']
        self.assertEqual(len(roles), 2)

    def test_delete_existent_role_id_case_insensitive(self):
        self.app.delete(self.api_url_p2 + "/MeMBER.json", status=HTTPOk.code)
        resp = self.app.get(self.api_url_p2, status=HTTPOk.code)
        roles = resp.json['result']
        self.assertEqual(len(roles), 2)


class ProjectRoleAPIProjectAdminPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = "/api/v1/project/roles/p1"
        self.api_url_p2 = "/api/v1/project/roles/p2"
        self.api_url_not_exist = "/api/v1/project/roles/p3"

        self.config = testing.setUp()
        self.config.include("cornice")
        request = testing.DummyRequest()
        root_factory = build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA)
        request.root = root_factory(None)
        self.config.testing_securitypolicy(
            userid="john", groupids=find_group('john', request))
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(root_factory)
        self.config.scan("ttrack.projects.api")
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def tearDown(self):
        testing.tearDown()

    def test_allow_get_collection(self):
        self.app.get(self.api_url_p1, status=HTTPOk.code)

    def test_deny_get_collection_on_non_member_project(self):
        self.app.get(self.api_url_p2, status=HTTPForbidden.code)

    def test_allow_get(self):
        self.app.get(self.api_url_p1 + '/member.json', status=HTTPOk.code)

    def test_deny_get_on_non_member_project(self):
        self.app.get(self.api_url_p2 + '/member.json',
                     status=HTTPForbidden.code)

    def test_allow_add(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p1, role, status=HTTPOk.code)


    def test_deny_add_on_non_member_project(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p2, role,
                           status=HTTPForbidden.code)

    def test_allow_update(self):
        role = {
            'role_id' : 'manager',
            'name' : 'My new Manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p1 + '/manager.json', role, status=HTTPOk.code)


    def test_deny_update_on_non_member_project(self):
        role = {
            'role_id' : 'manager',
            'name' : 'My new Manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p2 + '/manager.json', role,
            status=HTTPForbidden.code)

    def test_allow_delete(self):
        self.app.delete(self.api_url_p1 + "/manager.json", status=HTTPOk.code)

    def test_deny_delete_on_non_member_project(self):
        self.app.delete(self.api_url_p2 + "/manager.json",
                        status=HTTPForbidden.code)

class ProjectRoleAPIMemberPermissionsTests(unittest.TestCase):

    def setUp(self):
        self.api_url_p1 = "/api/v1/project/roles/p1"
        self.api_url_p2 = "/api/v1/project/roles/p2"
        self.api_url_not_exist = "/api/v1/project/roles/p3"

        self.config = testing.setUp()
        self.config.include("cornice")
        request = testing.DummyRequest()
        root_factory = build_root_factory(USER_DATA, CLIENT_DATA, PROJECT_DATA)
        request.root = root_factory(None)
        self.config.testing_securitypolicy(
            userid="john", groupids=find_group('doe', request))
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.config.set_root_factory(root_factory)
        self.config.scan("ttrack.projects.api")
        self.config.scan("ttrack.clients.api")
        self.app = TestApp(CatchErrors(self.config.make_wsgi_app()))

    def test_deny_collection_get(self):
        self.app.get(self.api_url_p2, status=HTTPForbidden.code)

    def test_deny_get(self):
        self.app.get(self.api_url_p2 + '/member.json', status=HTTPForbidden.code)

    def test_deny_add(self):
        role = {
            'role_id' : 'design',
            'name' : 'Design Engineer',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.post_json(self.api_url_p1, role,
                           status=HTTPForbidden.code)

    def test_deny_update(self):
        role = {
            'role_id' : 'manager',
            'name' : 'My new Manager',
            'permissions' : ['Add/Edit/View timelogs for self',]
        }
        self.app.put_json(
            self.api_url_p2 + '/manager.json', role,
            status=HTTPForbidden.code)

    def test_deny_delete(self):
        self.app.delete(self.api_url_p2 + "/manager.json",
                        status=HTTPForbidden.code)

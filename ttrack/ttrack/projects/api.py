# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.security import (
    authenticated_userid,
    has_permission
    )
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden
    )

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )
import colander

from .models import (
    Project, Activity, Member, ProjectRole, PROJECT_PERMISSIONS,
)
from . import validators
from ttrack import clients, users


class AddProjectSchema(colander.MappingSchema):
    project_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")
    client_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    userid = colander.SchemaNode( # Project admin userid
        colander.String(), location="body", type="str")

class PutProjectSchema(colander.MappingSchema):
    project_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)

class ActivitySchema(colander.MappingSchema):
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")

class MemberSchema(colander.MappingSchema):
    userid = colander.SchemaNode(
        colander.String(), location="body", type="str")
    project_role_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)

class Permissions(colander.SequenceSchema):
    permissions = colander.SchemaNode(
        colander.String(), location="body", type="str")

class ProjectRoleSchema(colander.MappingSchema):
    role_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")
    permissions = Permissions(validator=colander.Length(min=1))


@cornice_resource(
    name="api.projects",
    collection_path="/api/v1/projects",
    path="/api/v1/projects/{project_id}.json",
    permission="projects.manage",
    )
class API(object):
    def __init__(self, request):
        self.request = request
        self.projects = request.root['projects']
        self.clients = request.root['clients']
        self.users = request.root['users']

    # Helper methods
    def is_project_id_unique(self, request):
        project_id = request.json['project_id']
        # we should check only when there is a 'project_id'. missing
        # fields are handled by schema validation.
        if project_id and not self.projects.is_id_unique(project_id):
            self.request.errors.add('body', 'project_id', 'Project already exists')
            self.request.errors.status = HTTPBadRequest.code

    # Views
    @cornice_view(permission="projects.view")
    def collection_get(self):
        data = {
            "type" : 'project_list',
            "result" : None,
            }
        data['result'] = [project.to_dict()
                          for project in self.projects.values()]
        return data

    @cornice_view(schema=AddProjectSchema,
                  validators=('is_project_id_unique',
                              clients.validators.body_client_id_exists,
                              users.validators.body_userid_exists))
    def collection_post(self):
        new_project = {
            'project_id' : self.request.json.get('project_id'),
            'name' : self.request.json.get('name')
        }
        new_project['client'] = self.clients[self.request.json.get('client_id')]
        new_project['admin_user'] = self.users[self.request.json.get('userid')]

        project = Project.from_dict(new_project)
        self.projects.add(project)
        data = {
            'type' : 'project',
            'result' : project.to_dict()
            }
        return data

    @cornice_view(permission="projects.view",
                  validators=(validators.url_project_id_exists))
    def get(self):
        project_id = self.request.matchdict['project_id']
        project = self.projects.get(project_id)
        data = {
            'type' : "project",
            'result' : project.to_dict(),
            }
        return data

    @cornice_view(schema=PutProjectSchema,
                  validators=(clients.validators.body_client_id_exists,
                              validators.strict_url_project_id_exists))
    def put(self):
        project_id = self.request.matchdict['project_id']

        # enforce 'project_id' is read-only
        if project_id != self.request.json_body['project_id']:
            self.request.errors.add(
                'body', 'project_id', 'project_id is read-only')
            self.request.errors.status = HTTPBadRequest.code
            return

        project = self.projects.get(project_id)
        updated_project = self.request.json

        if "name" in updated_project:
            project.name = updated_project["name"]

        data = {
            'type' : 'project',
            'result' : project.to_dict(),
            }
        return data

    @cornice_view(validators=(validators.strict_url_project_id_exists,))
    def delete(self):
        route_project_id = self.request.matchdict['project_id']
        self.projects.remove(route_project_id)


@cornice_resource(
    name="api.activities",
    collection_path="/api/v1/activities/{project_id}",
    path="/api/v1/activities/{project_id}/{activity_name}.json",
    permission="view" # All permissions on this are context based so
                      # check them in their corresponding methods
)
class ActivitiesAPI(object):

    def __init__(self, request):
        self.request = request
        self.projects = request.root['projects']

    def get_activities(self):
        project_id = self.request.matchdict['project_id']
        project = self.projects[project_id]
        return project.activities

    def has_context_permission(self, perm_name):
        project = self.projects[self.request.matchdict['project_id']]
        userid = authenticated_userid(self.request)
        if not self.request.has_permission(perm_name, project):
            self.request.errors.add(
                'url', 'project_id',
                "User='%s' has no desired permission on the activity" % userid)
            self.request.errors.status = HTTPForbidden.code
            return False
        return True

    @cornice_view(validators=(validators.url_project_id_exists,))
    def collection_get(self):
        if not self.has_context_permission('activities.view'):
            return

        activities = self.get_activities()
        return {
            "type" : 'activity_list',
            "result" : [act.to_dict() for act in activities.values()]
        }

    @cornice_view(
        schema=ActivitySchema,
        validators=(validators.strict_url_project_id_exists,),
    )
    def collection_post(self):
        if not self.has_context_permission('activities.manage'):
            return

        activities = self.get_activities()
        # ensure unique
        name = self.request.json.get('name', None)
        if name in activities:
            self.request.errors.add('body', 'name',
                                    "Activity='%s' already exists" % name)
            self.request.errors.status = HTTPBadRequest.code
            return

        act = Activity(self.request.json['name'])
        activities.add(act)
        return {
            'type': 'activity',
            'result': act.to_dict()
        }

    @cornice_view(validators=(validators.url_project_id_exists,))
    def get(self):
        if not self.has_context_permission('activities.view'):
            return

        activities = self.get_activities()
        name = self.request.matchdict['activity_name']
        if name not in activities:
            self.request.errors.add('url', name, 'Does not exist')
            self.request.errors.status = HTTPNotFound.code
            return
        act = activities.get(name)
        return {
            'type' : 'activity',
            'result' : act.to_dict(),
        }

    @cornice_view(schema=ActivitySchema,
                  validators=(validators.strict_url_project_id_exists,))
    def put(self):
        if not self.has_context_permission('activities.manage'):
            return

        activities = self.get_activities()
        name = self.request.matchdict['activity_name']
        act = activities.get(name, None)
        if act is None:
            self.request.errors.add(
                'body', 'name', "No such activity='%s'" % (name,))
            self.request.errors.status = HTTPBadRequest.code
            return

        # ensure that the new activity_name is unique
        new_act_name = self.request.json['name']
        if ((new_act_name.lower() != name.lower()) and
            activities.exists(new_act_name)):
            self.request.errors.add(
                'body', 'name', "Activity='%s' already exists" % (new_act_name,))
            self.request.errors.status = HTTPBadRequest.code
            return

        activities.rename_activity_name(name, new_act_name)

        data = {
            'type' : 'activity',
            'result' : act.to_dict(),
        }
        return data

    @cornice_view(validators=(validators.strict_url_project_id_exists,))
    def delete(self):
        if not self.has_context_permission('activities.manage'):
            return

        activities = self.get_activities()
        name = self.request.matchdict['activity_name']
        act = activities.get(name, None)
        if act is None:
            self.request.errors.add(
                'url', 'name', "Activity='%s' does not exist" % (name,))
            self.request.errors.status = HTTPBadRequest.code
            return
        activities.remove(act.name)

@cornice_resource(
    name="api.project.roles",
    collection_path="/api/v1/project/roles/{project_id}",
    path="/api/v1/project/roles/{project_id}/{role_id}.json",
    permission="view"
)
class ProjectRolesAPI(object):

    def __init__(self, request):
        self.request = request
        self.projects = request.root['projects']

    def get_roles(self):
        project_id = self.request.matchdict['project_id']
        project = self.projects[project_id]
        return project.roles

    def is_permissions_valid(self):
        permissions = self.request.json.get('permissions', None)
        if permissions is None:
            return False
        for permission in permissions:
            if permission not in PROJECT_PERMISSIONS:
                self.request.errors.add(
                    'body', 'permissions', "Invalid permission (%s)" %
                    permission)
                self.request.errors.status = HTTPBadRequest.code
                return False
        return True

    def has_context_permission(self, perm_name):
        project = self.projects[self.request.matchdict['project_id']]
        userid = authenticated_userid(self.request)
        if not self.request.has_permission(perm_name, project):
            self.request.errors.add(
                'url', 'project_id',
                "User='%s' has no desired permission on the activity" % userid)
            self.request.errors.status = HTTPForbidden.code
            return False
        return True

    @cornice_view(validators=(validators.url_project_id_exists,))
    def collection_get(self):
        if not self.has_context_permission('project.roles.view'):
            return

        roles = self.get_roles()
        return {
            'type' : 'project_role_list',
            'result' : [role.to_dict() for role in roles.values()]
        }

    @cornice_view(validators=(validators.strict_url_project_id_exists,),
                  schema=ProjectRoleSchema)
    def collection_post(self):
        if not self.has_context_permission('project.roles.manage'):
            return

        roles = self.get_roles()
        role_id = self.request.json.get('role_id').strip().lower()
        if role_id in roles:
            self.request.errors.add(
                'body', 'role_id',
                'Project role ID (%s) already exists' % (role_id,))
            self.request.errors.status = HTTPBadRequest.code
            return
        if not self.is_permissions_valid():
            return

        role = ProjectRole.from_dict(self.request.json)
        roles.add(role)
        return {
            'type' : 'project_role_list',
            'result' : role.to_dict()
        }

    @cornice_view(validators=(validators.url_project_id_exists,))
    def get(self):
        if not self.has_context_permission('project.roles.view'):
            return

        roles = self.get_roles()
        role_id = self.request.matchdict['role_id'].strip().lower()
        if role_id not in roles:
            self.request.errors.add('url', role_id, 'Does not exist')
            self.request.errors.status = HTTPNotFound.code
            return

        role = roles.get(role_id)
        res = role.to_dict()

        return {
            'type' : 'project_role',
            'result' : res
        }

    @cornice_view(validators=(validators.strict_url_project_id_exists,),
                  schema=ProjectRoleSchema)
    def put(self):
        if not self.has_context_permission('project.roles.manage'):
            return

        role_id = self.request.matchdict['role_id'].strip().lower()
        if role_id != self.request.json.get('role_id').strip().lower():
            self.request.errors.add(
                'body', 'role_id', 'Project role_id is read-only')
            self.request.errors.status = HTTPBadRequest.code
            return

        if role_id == 'admin':
            self.request.errors.add(
                'url', role_id, 'Cannot modify the system project admin role')
            self.request.errors.status = HTTPBadRequest.code
            return

        roles = self.get_roles()
        if role_id not in roles:
            self.request.errors.add(
                'url', role_id,
                'Project role ID (%s) does not exist' % (role_id,))
            self.request.errors.status = HTTPBadRequest.code
            return

        role = roles.get(role_id)
        role.name = self.request.json.get('name')

        new_permissions = self.request.json.get('permissions')

        to_remove = []
        for permission in role.permissions:
            if permission not in new_permissions:
                to_remove.append(permission)

        for permission in to_remove:
            role.remove_permission(permission)

        for permission in new_permissions:
            if permission not in role.permissions:
                role.add_permission(permission)

        return {
            'type' : 'project_role',
            'result' : role.to_dict()
        }

    @cornice_view(validators=(validators.strict_url_project_id_exists,))
    def delete(self):
        if not self.has_context_permission('project.roles.manage'):
            return

        role_id = self.request.matchdict['role_id'].strip().lower()
        if role_id == 'admin':
            self.request.errors.add(
                'url', role_id, 'Cannot delete the system project admin role')
            self.request.errors.status = HTTPBadRequest.code
            return

        roles = self.get_roles()
        if role_id not in roles:
            self.request.errors.add(
                'url', role_id,
                'Project role ID (%s) does not exist' % (role_id,))
            self.request.errors.status = HTTPBadRequest.code
            return

        roles.remove(role_id)


@cornice_resource(
    name="api.members",
    collection_path="/api/v1/members/{project_id}",
    path="/api/v1/members/{project_id}/{userid}.json",
    permission="view" # All permissions on this are context based so
                      # check them in their corresponding methods
)
class MemberAPI(object):

    def __init__(self, request):
        self.request = request
        self.projects = request.root['projects']
        self.users = request.root['users']

    def has_context_permission(self, perm_name):
        project = self.projects[self.request.matchdict['project_id']]
        userid = authenticated_userid(self.request)
        if not self.request.has_permission(perm_name, project):
            self.request.errors.add(
                'url', 'project_id',
                "User='%s' has no desired permission on the member" % userid)
            self.request.errors.status = HTTPForbidden.code
            return False
        return True

    def get_members(self):
        project = self.projects[self.request.matchdict['project_id']]
        return project.members

    @cornice_view(
        validators=(validators.url_project_id_exists,))
    def collection_get(self):
        if not self.has_context_permission('members.view'):
            return

        members = self.get_members()
        return {
            'type' : 'project_member_list',
            'result' : [member.to_dict() for member in members.values()]
        }

    @cornice_view(
        schema=MemberSchema,
        validators=(validators.strict_url_project_id_exists,
                    users.validators.body_userid_exists))
    def collection_post(self):
        if not self.has_context_permission('members.manage'):
            return

        project = self.projects[self.request.matchdict['project_id']]

        userid = self.request.json['userid']
        if userid in project.members:
            self.request.errors.add(
                'body', 'userid', "User='%s' is already a member" % (userid,))
            self.request.errors.status = HTTPBadRequest.code
            return

        user = self.users.get(userid)
        member = Member(user)
        project_role_id = self.request.json.get('project_role_id', None)
        if project_role_id:
            if project_role_id not in project.roles:
                self.request.errors.add(
                    'body', 'projet_role_id',
                    'Project role id (%s) does not exist' % (project_role_id,))
                self.request.errors.status = HTTPBadRequest.code
                return
            member.role = project.roles.get(project_role_id)

        project.add_member(member)
        return {
            'type' : 'project_member',
            'result' : member.to_dict()
        }

    @cornice_view(validators=(validators.url_project_id_exists,
                              users.validators.url_userid_exists))
    def get(self):
        if not self.has_context_permission('members.view'):
            return

        members = self.get_members()
        userid = self.request.matchdict['userid']
        if userid not in members:
            self.request.errors.add(
                'url', 'userid', "User='%s' is not in member list" % (userid,))
            self.request.errors.status = HTTPNotFound.code
            return
        member = members.get(userid)
        return {
            'type' : 'project_member',
            'result' : member.to_dict()
        }

    @cornice_view(
        schema=MemberSchema,
        validators=(validators.strict_url_project_id_exists,
                    users.validators.body_userid_exists))
    def put(self):
        if not self.has_context_permission('members.manage'):
            return

        project = self.projects[self.request.matchdict['project_id']]
        userid = self.request.matchdict['userid'].strip().lower()
        if userid not in project.members:
            self.request.errors.add(
                'url', 'userid', "User='%s' is not a member" % (userid,))
            self.request.errors.status = HTTPBadRequest.code
            return

        if userid != self.request.json.get('userid').strip().lower():
            self.request.errors.add(
                'body', 'userid', "Project member's userid is read-only")
            self.request.errors.status = HTTPBadRequest.code
            return

        member = project.members.get(userid)

        project_role_id = self.request.json.get('project_role_id', None)
        if project_role_id: # this validates both None and ''
            if project_role_id not in project.roles:
                self.request.errors.add(
                    'body', 'project_role_id',
                    'Project role id (%s) does not exist' % (project_role_id,))
                self.request.errors.status = HTTPBadRequest.code
                log.debug("Throw error")
                return
            member.role = project.roles.get(project_role_id)
        else:
            member.role = None

        return {
            'type' : 'project_member',
            'result' : member.to_dict()
        }



    @cornice_view(validators=(validators.strict_url_project_id_exists,
                              users.validators.url_userid_exists))
    def delete(self):
        if not self.has_context_permission('members.manage'):
            return

        project = self.projects[self.request.matchdict['project_id']]
        userid = self.request.matchdict['userid']
        if userid not in project.members:
            self.request.errors.add(
                'url', 'userid', "User='%s' is not in member list" % (userid,))
            self.request.errors.status = HTTPNotFound.code
            return
        member = project.members.get(userid)
        project.remove_member(member)

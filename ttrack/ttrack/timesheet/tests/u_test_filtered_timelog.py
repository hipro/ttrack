# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import unittest
import mock
import datetime


def join_params(*args):
    return '.'.join(args)


def build_timesheet_mock():
    from ttrack.timesheet.models import timesheet
    ts = timesheet.TimeSheet()
    ts.get_project_timelog = mock.Mock(side_effect=join_params)
    ts.get_project_user_timelog = mock.Mock(side_effect=join_params)
    ts.get_project_tag_timelog = mock.Mock(side_effect=join_params)
    ts.get_activity_timelog = mock.Mock(side_effect=join_params)
    ts.get_activity_tag_timelog = mock.Mock(side_effect=join_params)
    ts.get_user_activity_timelog = mock.Mock(side_effect=join_params)
    ts.get_client_timelog = mock.Mock(side_effect=join_params)
    ts.get_client_user_timelog = mock.Mock(side_effect=join_params)
    ts.get_client_tag_timelog = mock.Mock(side_effect=join_params)
    return ts


@mock.patch(
    'ttrack.timesheet.models.timesheet.intersecting_timelog', join_params)
class ProjectFilteredTimeLogTests(unittest.TestCase):
    def setUp(self):
        self.timesheet = build_timesheet_mock()

    def tearDown(self):
        pass

    def test_no_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', None, None, None)
        self.assertEqual(tl, 'p')

    def test_user_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', 'u', None, None)
        self.assertEqual(tl, 'p.u')

    def test_user_activity_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', 'u', 'a', None)
        self.assertEqual(tl, 'u.a')

    def test_user_tag_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', 'u', None, 't')
        self.assertEqual(tl, 'p.u.p.t')

    def test_user_activity_tag_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', 'u', 'a', 't')
        self.assertEqual(tl, 'u.a.p.t')

    def test_activity_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', None, 'a', None)
        self.assertEqual(tl, 'a')

    def test_activity_tag_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', None, 'a', 't')
        self.assertEqual(tl, 'a.t')

    def test_tag_filter(self):
        tl = self.timesheet.get_project_filtered_timelog('p', None, None, 't')
        self.assertEqual(tl, 'p.t')


@mock.patch(
    'ttrack.timesheet.models.timesheet.intersecting_timelog', join_params)
class ClientFilteredTimeLogTests(unittest.TestCase):
    def setUp(self):
        self.timesheet = build_timesheet_mock()

    def tearDown(self):
        pass

    def test_no_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', None, None, None)
        self.assertEqual(tl, 'c')

    def test_project_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', 'p', None, None)
        self.assertEqual(tl, 'p')

    def test_project_user_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', 'p', 'u', None)
        self.assertEqual(tl, 'p.u')

    def test_project_tag_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', 'p', None, 't')
        self.assertEqual(tl, 'p.t')

    def test_project_user_tag_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', 'p', 'u', 't')
        self.assertEqual(tl, 'p.u.p.t')

    def test_user_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', None, 'u', None)
        self.assertEqual(tl, 'c.u')

    def test_user_tag_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', None, 'u', 't')
        self.assertEqual(tl, 'c.u.c.t')

    def test_tag_filter(self):
        tl = self.timesheet.get_client_filtered_timelog('c', None, None, 't')
        self.assertEqual(tl, 'c.t')

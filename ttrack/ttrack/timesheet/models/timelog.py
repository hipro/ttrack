# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime
import decimal
from decimal import Decimal

from persistent import Persistent
from BTrees.OOBTree import OOBTree, intersection

from ttrack import errors
from .timeentry import (
    compute_timeentry_id,
    FIRST_UUID,
    LAST_UUID,
)


def _to_mins(delta):
    mins = int(delta.total_seconds() / 60)
    return mins

def _to_hours(delta):
    hours = Decimal(delta.total_seconds() / (60 * 60))
    hours = hours.quantize(Decimal(".01"), rounding=decimal.ROUND_UP)
    return hours

def _compute_first_last_keys(from_datetime, to_datetime):
    start_key = None
    if from_datetime:
        start_key = compute_timeentry_id(from_datetime, UUID=FIRST_UUID)
    end_key = None
    if to_datetime:
        end_key = compute_timeentry_id(to_datetime, UUID=LAST_UUID)
    return (start_key, end_key)


class TimeLog(Persistent):
    __parent__ = __name__ = None

    def __init__(self):
        super(TimeLog, self).__init__()
        self._ute_logs = OOBTree()
        self._ite_logs = OOBTree()
        self._total_times = OOBTree()
        self._total_time = datetime.timedelta()

    def add(self, ute):
        ret = self._ute_logs.insert(ute.ute_id, ute)
        if ret == 0:
            raise errors.BadParameter(
                "Timelog ute id='%s' already exists" % (ute.ute_id,))
        for ite in ute.ite_list:
            ret = self._ite_logs.insert(ite.ite_id, ite)
            if ret == 0:
                raise errors.BadParamter(
                    "Timelog ite id='%s' already exists" % (ite.ite_id,))
            total_time_keys = self._compute_total_time_keys(ite)
            for key in total_time_keys:
                td = self._total_times.get(key, datetime.timedelta())
                td += ite.effective_time()
                self._total_times[key] = td
            self._total_time += ite.effective_time()

    def remove(self, ute):
        for ite in ute.ite_list:
            del(self._ite_logs[ite.ite_id])
            total_time_keys = self._compute_total_time_keys(ite)
            for key in total_time_keys:
                td = self._total_times[key]
                td -= ite.effective_time()
                self._total_times[key] = td
            self._total_time -= ite.effective_time()
        del(self._ute_logs[ute.ute_id])

    def get_ite_keys(self, from_datetime, to_datetime):
        start_key, end_key = _compute_first_last_keys(
            from_datetime, to_datetime)
        return self._ite_logs.keys(min=start_key, max=end_key)

    def get_ute_keys(self, from_datetime, to_datetime):
        start_key, end_key = _compute_first_last_keys(
            from_datetime, to_datetime)
        return self._ute_logs.keys(min=start_key, max=end_key)

    def get_ite_entries(self, from_datetime, to_datetime):
        """Get ITE objects in given datetime range"""
        start_key, end_key = _compute_first_last_keys(
            from_datetime, to_datetime)
        return self._ite_logs.values(min=start_key, max=end_key)

    def get_ute_entries(self, from_datetime, to_datetime):
        """Get UTE objects in given datetime range"""
        start_key, end_key = _compute_first_last_keys(
            from_datetime, to_datetime)
        return self._ute_logs.values(min=start_key, max=end_key)

    def get_ute(self, key, *args):
        return self._ute_logs.get(key, *args)

    def get_ite(self, key, *args):
        return self._ite_logs.get(key, *args)

    def ute_size(self):
        return len(self._ute_logs)

    def ite_size(self):
        return len(self._ite_logs)

    def empty(self):
        return (len(self._ute_logs) == 0)

    def get_total_time_delta(self, key=None):
        delta = self._total_time
        if key:
            delta = self._total_times.get(key, datetime.timedelta())
        return delta

    def get_total_time_mins(self, key=None):
        delta = self.get_total_time_delta(key=key)
        return _to_mins(delta)

    def get_total_time_hours(self, key=None):
        delta = self.get_total_time_delta(key=key)
        return _to_hours(delta)

    def _compute_total_time_keys(self, ite):
        year_key = ite.start_time.strftime("year-%Y")
        month_key = ite.start_time.strftime("month-%Y-%m")
        isocal = ite.start_time.isocalendar()
        week_key = "week-%04d-%02d" % (isocal[0], isocal[1])
        return (year_key, month_key, week_key)

    def get_total_time_for_range_delta(self, start_ts, end_ts):
        ite_entries = self.get_ite_entries(start_ts, end_ts)
        delta = datetime.timedelta()
        for ite in ite_entries:
            delta += ite.effective_time()
        return delta

    def get_total_time_for_range_mins(self, start_ts, end_ts):
        delta = self.get_total_time_for_range_delta(start_ts, end_ts)
        return _to_mins(delta)

    def get_total_time_for_range_hours(self, start_ts, end_ts):
        delta = self.get_total_time_for_range_delta(start_ts, end_ts)
        return _to_hours(delta)


class DynamicTimeLog(object):
    """A wrapper to handle a subset of keys from the TimeLog

    The 'selected_keys' need not be contiguous in the given
    'timelog'. However, all keys in 'selected_keys' exist in the given
    'timelog'.

    """

    def __init__(self, timelog, selected_keys):
        self._timelog = timelog
        self._selected_keys = selected_keys

    def get_ite_keys(self, from_datetime, to_datetime):
        start_key, end_key = _compute_first_last_keys(
            from_datetime, to_datetime)
        return self._selected_keys.keys(min=start_key, max=end_key)

    def get_ite_entries(self, from_datetime, to_datetime):
        key_set = self.get_ite_keys(from_datetime, to_datetime)
        entries = []
        for key in key_set:
            entries.append(self._timelog.get_ite(key))
        return entries

    def get_ite(self, key, *args):
        return self._timelog.get_ite(key, *args)

    def ite_size(self):
        return len(self._selected_keys)

    def empty(self):
        return (len(self._selected_keys) == 0)

    def get_total_time_for_range_delta(self, from_datetime, to_datetime):
        ite_key_list = self.get_ite_keys(from_datetime, to_datetime)
        delta = datetime.timedelta()
        for ite_key in ite_key_list:
            ite = self.get_ite(ite_key)
            delta += ite.effective_time()
        return delta

    def get_total_time_for_range_mins(self, start_ts, end_ts):
        delta = self.get_total_time_for_range_delta(start_ts, end_ts)
        return _to_mins(delta)

    def get_total_time_for_range_hours(self, start_ts, end_ts):
        delta = self.get_total_time_for_range_delta(start_ts, end_ts)
        return _to_hours(delta)


def intersecting_timelog(timelog_a, timelog_b):
    key_set = intersection(timelog_a._ite_logs, timelog_b._ite_logs)
    timelog = DynamicTimeLog(timelog_a, key_set)
    return timelog

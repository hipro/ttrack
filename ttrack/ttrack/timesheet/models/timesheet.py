# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from persistent import Persistent
from persistent.mapping import PersistentMapping
from BTrees.OOBTree import OOBTree

from ttrack import errors

from .timeentry import (
    UITimeEntry,
    InternalTimeEntry,
)

from .timelog import (
    TimeLog,
    intersecting_timelog,
)

from .project_timesheet import ProjectTimeSheet, ActivityTimeSheet
from .client_timesheet import ClientTimeSheet
from .user_timesheet import UserTimeSheet
from .tag_timesheet import TagTimeSheet


class TimeSheet(Persistent):
    """Global time-sheet"""

    __parent__ = __name__ = None

    def __init__(self):
        super(TimeSheet, self).__init__()
        self.__all = TimeLog()
        self.__users = PersistentMapping()
        self.__projects = PersistentMapping()
        self.__activities = PersistentMapping()
        self.__tags = PersistentMapping()
        self.__clients = PersistentMapping()

    def calculate_fit(self, ute, exclude=None):
        """Given an UTE, returns good/error/split states"""

        user_ts = self.__users[ute.user]
        overlapping = get_overlapping_entries(
            user_ts.get_all(), ute.start_datetime, ute.end_datetime, exclude)
        if not overlapping:
            return ("good", None)
        if overlapping[0][0] is "conflict":
            return ("error", overlapping[0][1])
        to_add = fit_time_entry(ute, overlapping)
        if not to_add:
            e_list = []
            for status, ent in overlapping:
                e_list.append(ent)
            return ("error", e_list)
        return ("split", to_add)

    def add(self, timeentry):
        """Add a single timeentry object in the time sheet."""

        self.__all.add(timeentry)
        self.__users[timeentry.user].add(timeentry)
        self.__projects[timeentry.project].add(timeentry)
        self.__activities[timeentry.activity].add(timeentry)
        self.__clients[timeentry.project.client].add(timeentry)
        for tag in timeentry.tags:
            self.__tags[tag].add(timeentry)

    def remove(self, timeentry):
        self.__all.remove(timeentry)
        self.__users[timeentry.user].remove(timeentry)
        self.__projects[timeentry.project].remove(timeentry)
        self.__activities[timeentry.activity].remove(timeentry)
        self.__clients[timeentry.project.client].remove(timeentry)
        for tag in timeentry.tags:
            self.__tags[tag].remove(timeentry)

    def add_user(self, user):
        self.__users[user] = UserTimeSheet()

    def add_project(self, project):
        self.__projects[project] = ProjectTimeSheet()

    def add_activity(self, activity):
        self.__activities[activity] = ActivityTimeSheet()

    def add_tag(self, tag):
        self.__tags[tag] = TagTimeSheet()

    def add_client(self, client):
        self.__clients[client] = ClientTimeSheet()

    def remove_user(self, user):
        tl = self.__users[user].get_all()
        for timeentry in tl.get_ute_entries(None, None):
            self.__all.remove(timeentry)
            self.__projects[timeentry.project].remove(timeentry)
            self.__activities[timeentry.activity].remove(timeentry)
            self.__clients[timeentry.project.client].remove(timeentry)
            for tag in timeentry.tags:
               self.__tags[tag].remove(timeentry)
        del(self.__users[user])

    def remove_project(self, project):
        for activity in project.activities:
            self.remove_activity(activity)
        del(self.__projects[project])

    def remove_activity(self, activity):
        tl = self.__activities[activity].get_all()
        for timeentry in tl.get_ute_entries(None, None):
            self.__all.remove(timeentry)
            self.__users[timeentry.user].remove(timeentry)
            self.__projects[timeentry.project].remove(timeentry)
            self.__clients[timeentry.project.client].remove(timeentry)
            for tag in timeentry.tags:
               self.__tags[tag].remove(timeentry)
        del(self.__activities[activity])

    def remove_client(self, client):
        for project in client.projects:
            self.remove_project(project)
        del(self.__clients[client])

    def remove_tag(self, tag):
        # Tags a special case. They are not mandatory fields. So, we
        # need to just unlink them from the 'TimeEntry' object. The
        # categorised TimeSheets have their own 'tag specific'
        # time-logs. They have to be notified too.
        tl = self.__tags[tag].get_all()
        for timeentry in tl.get_ute_entries(None, None):
            timeentry.unlink_tag(tag)
            self.__users[timeentry.user].remove_tag(tag)
            self.__projects[timeentry.project].remove_tag(tag)
            self.__activities[timeentry.activity].remove_tag(tag)
            self.__clients[timeentry.project.client].remove_tag(tag)
        del(self.__tags[tag])


    def get_all_timelog(self):
        return self.__all

    def get_user_timelog(self, user):
        ts = self.__users[user]
        return ts.get_all()

    def get_tag_timelog(self, tag):
        ts = self.__tags[tag]
        return ts.get_all()

    def get_user_activity_timelog(self, user, activity):
        ts = self.__users[user]
        return ts.get_activity(activity)

    def get_user_tag_timelog(self, user, tag):
        ts = self.__users[user]
        return ts.get_tag(tag)

    def get_project_timelog(self, project):
        ts = self.__projects[project]
        return ts.get_all()

    def get_project_user_timelog(self, project, user):
        ts = self.__projects[project]
        return ts.get_user(user)

    def get_project_tag_timelog(self, project, tag):
        ts = self.__projects[project]
        return ts.get_tag(tag)

    def get_activity_timelog(self, activity):
        ts = self.__activities[activity]
        return ts.get_all()

    def get_activity_tag_timelog(self, activity, tag):
        ts = self.__activities[activity]
        return ts.get_tag(tag)

    def get_client_timelog(self, client):
        ts = self.__clients[client]
        return ts.get_all()

    def get_client_user_timelog(self, client, user):
        ts = self.__clients[client]
        return ts.get_user(user)

    def get_client_tag_timelog(self, client, tag):
        ts = self.__clients[client]
        return ts.get_tag(tag)

    def get_project_timesheet(self, project):
        return self.__projects[project]

    def get_user_timesheet(self, user):
        return self.__users[user]

    def get_client_timesheet(self, client):
        return self.__clients[client]

    def get_tag_timesheet(self, tag):
        return self.__tags[tag]

    def get_project_filtered_timelog(self, project, user, activity, tag):
        """Get timelog for (project && user && activity && tag)"""

        if user:
            if activity:
                ua_tl = self.get_user_activity_timelog(user, activity)
                if tag is None:
                    return ua_tl
                pt_tl = self.get_project_tag_timelog(project, tag)
                return intersecting_timelog(ua_tl, pt_tl)
            elif tag:
                pu_tl = self.get_project_user_timelog(project, user)
                pt_tl = self.get_project_tag_timelog(project, tag)
                return intersecting_timelog(pu_tl, pt_tl)
            else:
                return self.get_project_user_timelog(project, user)

        if activity:
            if tag:
                return self.get_activity_tag_timelog(activity, tag)
            else:
                return self.get_activity_timelog(activity)

        if tag:
            return self.get_project_tag_timelog(project, tag)

        return self.get_project_timelog(project)


    def get_client_filtered_timelog(self, client, project, user, tag):
        """Get timelog for (client && project && user && tag)"""
        if project:
            if user:
                if tag:
                    pu_tl = self.get_project_user_timelog(project, user)
                    pt_tl = self.get_project_tag_timelog(project, tag)
                    return intersecting_timelog(pu_tl, pt_tl)
                else:
                    return self.get_project_user_timelog(project, user)
            elif tag:
                return self.get_project_tag_timelog(project, tag)
            else:
                return self.get_project_timelog(project)

        if user:
            if tag:
                cu_tl = self.get_client_user_timelog(client, user)
                ct_tl = self.get_client_tag_timelog(client, tag)
                return intersecting_timelog(cu_tl, ct_tl)
            else:
                return self.get_client_user_timelog(client, user)

        if tag:
            return self.get_client_tag_timelog(client, tag)

        return self.get_client_timelog(client)

    def get_tag_filtered_timelog(self, tag, user, client, project):
        """Get timelog for (tag && user && client && project)"""
        if project and client and project.client != client:
            raise errors.BadParameter(
                "project.client=%s does not match client=%s" % \
                (project.client.client_id, client.client_id))

        if user:
            if client:
                if project:
                    ut_tl = self.get_user_tag_timelog(user, tag)
                    pt_tl = self.get_project_tag_timelog(project, tag)
                    return intersecting_timelog(ut_tl, pt_tl)
                cu_tl = self.get_client_user_timelog(client, user)
                ct_tl = self.get_client_tag_timelog(client, tag)
                return intersecting_timelog(cu_tl, ct_tl)
            return self.get_user_tag_timelog(user, tag)

        if project:
            return self.get_project_tag_timelog(project, tag)

        if client:
            return self.get_client_tag_timelog(client, tag)
        return self.get_tag_timelog(tag)

    def get_user_filtered_timelog(self, user, client, project, tag):
        """Get timelog for (user && client && project && tag)"""

        if project and client and project.client != client:
            raise errors.BadParameter(
                "project.client=%s does not match client=%s" % \
                (project.client.client_id, client.client_id))

        if project:
            if tag:
                pu_tl = self.get_project_user_timelog(project, user)
                pt_tl = self.get_project_tag_timelog(project, tag)
                return intersecting_timelog(pu_tl, pt_tl)
            else:
                return self.get_project_user_timelog(project, user)

        if client:
            if tag:
                cu_tl = self.get_client_user_timelog(client, user)
                ct_tl = self.get_client_tag_timelog(client, tag)
                return intersecting_timelog(cu_tl, ct_tl)
            else:
                return self.get_client_user_timelog(client, user)

        if tag:
            return self.get_user_tag_timelog(user, tag)

        return self.get_user_timelog(user)


def fit_time_entry(time_entry, overlap_list):
    split_list = []
    curr = UITimeEntry.clone(time_entry)

    for overlap, old in overlap_list:
        if old.start_datetime > curr.start_datetime:
            split = UITimeEntry.clone(curr)
            split.end_datetime = old.start_datetime
            split_list.append(split)
            curr.start_datetime = old.end_datetime
        elif old.start_datetime == curr.start_datetime:
            curr.start_datetime = old.end_datetime
        elif old.start_datetime < curr.start_datetime:
            curr.start_datetime = old.end_datetime

    if curr.start_datetime < curr.end_datetime:
        split_list.append(curr)

    return split_list

def get_overlapping_entries(timelog, start_datetime, end_datetime, exclude=None):
    search_start_ts = datetime.datetime.combine(
        start_datetime.date(), datetime.time(0, 0, 0))
    search_end_ts = datetime.datetime.combine(
        end_datetime.date(), datetime.time(23, 59, 0))

    ite_entries = timelog.get_ite_entries(search_start_ts, search_end_ts)
    overlap_list = []
    conflict_set = set()
    ute_set = []
    for old in ite_entries:
        if (old.start_time >= end_datetime) or \
           (old.end_time <= start_datetime):
            continue
        if old.ute.ute_id == exclude:
            continue
        if old.ute in ute_set:
            continue
        ute_set.append(old.ute)

    for old in ute_set:
        if old.start_datetime == start_datetime:
            if old.end_datetime > end_datetime:
                conflict_set.add(old)
                continue
            if old.end_datetime == end_datetime:
                conflict_set.add(old)
                continue
            if old.end_datetime < end_datetime:
                overlap_list.append(('contains', old))
                continue

        if old.start_datetime < start_datetime:
            if old.end_datetime > end_datetime:
                conflict_set.add(old)
                continue
            if old.end_datetime == end_datetime:
                conflict_set.add(old)
                continue
            if old.end_datetime < end_datetime:
                overlap_list.append(('post', old))
                continue

        if old.start_datetime > start_datetime:
            if old.end_datetime > end_datetime:
                overlap_list.append(('pre', old))
                continue
            if old.end_datetime == end_datetime:
                overlap_list.append(('contains', old))
                continue
            if old.end_datetime < end_datetime:
                overlap_list.append(('contains', old))
                continue
    if len(conflict_set) > 0:
        return (('conflict', list(conflict_set)),)
    return overlap_list

# Copyright (C) 2013 HiPro IT Solutions Private Limited. All rights
# reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import colander
from pyramid.httpexceptions import HTTPBadRequest
from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from ttrack import users, projects, tags
from .common import TimeSheetAPIBase

class ProjectCollectionGetSchema(colander.MappingSchema):
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int",
        missing=colander.drop, validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int",
        missing=colander.drop, validator=colander.Range(min=1, max=1000))
    start_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)
    end_date = colander.SchemaNode(
        colander.Date(), location="querystring", type="str",
        missing=colander.drop)
    userid = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    activity = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    tag_name = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)


@cornice_resource(
    name="api.project_timesheet",
    path="/api/v1/project_timesheet/{project_id}",
    permission="timesheet.view",
    )
class ProjectTimesheetAPI(TimeSheetAPIBase):

    def _validate_activity_name(self, request):
        project_id = request.matchdict['project_id']
        project = self.projects.get(project_id, None)
        if project is None:
            # We dont care about non-existant project_id. That is
            # someone else's problem.
            return
        activity_name = request.validated.get('activity', None)
        if activity_name is None:
            return
        activity = project.get_activity(activity_name, None)
        if activity is None:
            request.errors.add('querystring', 'activity',
                               'Activity="%s" does not exist' % \
                               (activity_name,))
            request.errors.status = HTTPBadRequest.code


    @cornice_view(schema=ProjectCollectionGetSchema,
                  validators=(projects.validators.url_project_id_exists,
                              users.validators.qs_userid_exists,
                              tags.validators.qs_tag_name_exists,
                              '_validate_activity_name',
                              '_validate_pagination_params_available',
                          ))
    def get(self):
        project = self._url_get_project()          # mandatory
        activity = self._qs_get_activity(project)  # optional
        user = self._qs_get_user()                 # optional
        tag = self._qs_get_tag()                   # optional

        return self._get_filtered_data(
            self.timesheet.get_project_filtered_timelog,
            project, user, activity, tag)

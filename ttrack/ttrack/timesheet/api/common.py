# Copyright (C) 2013 HiPro IT Solutions Private Limited. All rights
# reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from pyramid.security import (
    authenticated_userid,
    )
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden
    )
from ttrack.utilities import PaginatedContainer


class TimeSheetAPIBase(object):
    def __init__(self, request):
        self.request = request
        self.clients = request.root['clients']
        self.users = request.root['users']
        self.tags = request.root['tags']
        self.projects = request.root['projects']
        self.timesheet = request.root['timesheet']
        self.current_user = self.users[authenticated_userid(request)]


    def _url_get_user(self):
        return self.users.get(self.request.matchdict['userid'])

    def _url_get_client(self):
        return self.clients.get(self.request.matchdict['client_id'])

    def _url_get_project(self):
        return self.projects.get(self.request.matchdict['project_id'])

    def _url_get_tag(self):
        return self.tags.get(self.request.matchdict['tag_name'])


    def _qs_get_user(self):
        userid = self.request.validated.get('userid', None)
        if userid:
            return self.users.get(userid)
        return None

    def _qs_get_client(self):
        client_id = self.request.validated.get('client_id', None)
        if client_id:
            return self.clients.get(client_id)
        return None

    def _qs_get_project(self):
        project_id = self.request.validated.get('project_id', None)
        if project_id:
            return self.projects.get(project_id)
        return None

    def _qs_get_activity(self, project):
        activity_name = self.request.validated.get('activity', None)
        if activity_name:
            return project.get_activity(activity_name)
        return None

    def _qs_get_tag(self):
        tag_name = self.request.validated.get('tag_name', None)
        if tag_name:
            return self.tags.get(tag_name)
        return None


    def _get_filtered_data(self, filter_func,
                           param_1, param_2, param_3, param_4):
        request = self.request
        page = request.validated.get('page', None)
        page_size = request.validated.get("page_size", None)

        start_date = request.validated.get('start_date', None)
        if start_date:
            start_date = datetime.datetime.combine(
                start_date, datetime.time(0, 0))
        end_date = request.validated.get('end_date', None)
        if end_date:
            end_date = datetime.datetime.combine(
                end_date, datetime.time(23, 59, 59))

        timelog = filter_func(param_1, param_2, param_3, param_4)
        entries = None

        data = {}
        if page:
            container = PaginatedContainer(
                timelog.get_ite_entries(start_date, end_date), page_size)
            if not container.is_valid(page):
                request.errors.add(
                    'querystring', 'page',
                    'Page number (%d) out of range' % page)
                request.errors.status = HTTPBadRequest.code
                return
            entries = container.get(page)

            data['current_page'] = page
            data['page_size'] = page_size
            data['total_pages'] = container.page_count
            data['total_size'] = container.size
        else:
            entries = timelog.get_ite_entries(start_date, end_date)
            data['total_size'] = len(entries)

        timelog_data = [e.to_dict() for e in entries]

        data['type'] = 'timelog'
        data['result'] = timelog_data
        return data


    def _validate_pagination_params_available(self, request):
        page = self.request.params.get('page', None)
        page_size = self.request.params.get("page_size", None)
        if bool(page) != bool(page_size):
            self.request.errors.add(
                'querystring', 'page',
                'Both page and page_size are required for pagination')
            self.request.errors.status = HTTPBadRequest.code
            return

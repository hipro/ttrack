# Copyright (C) 2013 HiPro IT Solutions Private Limited. All rights
# reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import datetime

from pyramid.security import (
    authenticated_userid,
    has_permission
    )
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPForbidden
    )

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from cornice import Service

import colander

from ttrack import errors, users, projects, tags, clients
from ttrack.utilities import PaginatedContainer, PaginatedTimelogContainer

from ttrack.timesheet.models import UITimeEntry, TimeLog
from ttrack.timesheet import validators
from BTrees.OOBTree import OOSet, union


class SplitActionSchema(colander.MappingSchema):
    start_date = colander.SchemaNode(
        colander.Date(), location="body", type="str")
    start_time = colander.SchemaNode(
        colander.Time(), location="body", type="str")

    end_date = colander.SchemaNode(
        colander.Date(), location="body", type="str")
    end_time = colander.SchemaNode(
        colander.Time(), location="body", type="str")

    userid = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    exclude = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)

class TimeDetailSchema(colander.MappingSchema):
    start_date = colander.SchemaNode(
        colander.Date(), location="body", type="str")
    start_time = colander.SchemaNode(
        colander.Time(), location="body", type="str")

    end_date = colander.SchemaNode(
        colander.Date(), location="body", type="str")
    end_time = colander.SchemaNode(
        colander.Time(), location="body", type="str")

class TimeSplits(colander.SequenceSchema):
    time_detail = TimeDetailSchema()

class Tags(colander.SequenceSchema):
    tags = colander.SchemaNode(
        colander.String(), location="body", type="str")

class AddTimeEntriesSchema(colander.MappingSchema):
    time_splits = TimeSplits(validator=colander.Length(min=1))
    working_hours_only = colander.SchemaNode(
        colander.Boolean(), location="body", type="bool", default=False)
    ignore_conflicts = colander.SchemaNode(
        colander.Boolean(), location="body", type="bool", default=False)
    userid = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    project_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    activity = colander.SchemaNode(
        colander.String(), location="body", type="str")
    tags = Tags(missing=colander.drop)
    description = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)

class CollectionGetSchema(colander.MappingSchema):
    userid = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=1,
        required=False,
        validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=10,
        required=False,
        validator=colander.Range(min=1, max=1000))


@cornice_resource(
    name="api.timesheet",
    collection_path="/api/v1/timesheet",
    path="/api/v1/timesheet/{timeentry_id}.json",
    permission="timesheet.view",
    )
class API(object):
    def __init__(self, request):
        self.request = request
        self.users = request.root['users']
        self.projects = request.root['projects']
        self.tags = request.root['tags']
        self.current_user = self.users[authenticated_userid(request)]
        self.timesheet = request.root['timesheet']

    # Validators
    def is_time_splits_valid(self, time_splits):
        for ts in time_splits:
            try:
                te = TimeEntry.from_dict(ts)
            except errors.BadParameter, e:
                self.request.errors.add('body', 'start_time', str(e))
                self.request.errors.status = HTTPBadRequest.code
                return False
        return True

    def has_context_permission(self, perm_name, context):
        if not self.request.has_permission(perm_name, context):
            self.request.errors.add(
                'url', 'timeentry_id',
                "User='%s' has no desired permission on timeentry" % (
                    self.current_user.userid,))
            self.request.errors.status = HTTForbidden.code
            return False
        return True

    def construct_tags(self):
        tags = self.request.json.get('tags', None)
        # If the given tag name is not found create a new 'Tag' and
        # notify the event
        tag_list = []
        if tags:
            for tag_name in tags:
                tag = self.tags.get(tag_name, None)
                if tag is None:
                    tag = self.tags.add_by_name(tag_name)
                    # todo: this should be using pub-sub
                    # create TimeLog for the encountered new tag
                    self.timesheet.add_tag(tag)
                tag_list.append(tag)
        return tag_list

    def construct_activity(self, project):
        # If the given activity name is not found create a new
        # 'Activity' and notify the event
        act_name = self.request.json['activity']
        activity = project.activities.get(act_name, None)
        if activity is None:
            activity = project.activities.add_by_name(act_name)
            # todo: this should be using pub-sub
            # create TimeLog for the encountered new activity
            self.timesheet.add_activity(activity)
        return activity

    @cornice_view(validators=(validators.url_timeentry_id_exists,))
    def get(self):
        tl = self.timesheet.get_all_timelog()
        te = tl.get_ute(self.request.matchdict['timeentry_id'])

        if not has_context_permission('timesheet.view', te.project):
            return

        return {
            'type' : 'time_entry',
            'result' : te.to_dict(),
            }

    @cornice_view(schema=AddTimeEntriesSchema,
                  validators=(validators.strict_url_timeentry_id_exists,
                              users.validators.body_userid_exists,
                              projects.validators.body_project_id_exists))
    def put(self):
        tl = self.timesheet.get_all_timelog()
        old_te = tl.get_ute(self.request.matchdict['timeentry_id'])

        perm = 'timesheet.manage'
        if old_te.user != self.current_user:
            perm = 'timesheet.manage.others'
        if not self.has_context_permission(perm, old_te.project):
            return

        # Get the User object from request
        userid = self.request.json.get('userid', None)
        if userid:
            user = self.users.get(userid)
        else:
            user = self.current_user

        project = self.projects.get(self.request.json.get('project_id'))
        if old_te.project != project:
            # Project has been changed, so re-check the permission for
            # new project
            perm = 'timesheet.manage'
            if user != self.current_user:
                perm = 'timesheet.manage.others'
            if not self.has_context_permission(perm, project):
                return

        ignore_conflicts = self.request.validated['ignore_conflicts']
        working_hours_only = self.request.validated['working_hours_only']
        time_entries = []
        for ts in self.request.json['time_splits']:
            try:
                te = UITimeEntry.from_dict(ts)
            except errors.BadParameter, e:
                self.request.errors.add('body', 'start_time', str(e))
                self.request.errors.status = HTTPBadRequest.code
                return
            te.user = user
            if ignore_conflicts is False:
                status, entries = self.timesheet.calculate_fit(te, timeentry_id)
                if status != "good":
                    self.request.errors.add(
                        'body', 'time_splits',
                        "Conflicting entry in time splits found")
                    self.request.errors.status = HTTPBadRequest.code
                    return
            time_entries.append(te)

        # remove the old TimeEntry object and add the new timeentry list
        self.timesheet.remove(old_te)
        entries = []
        tag_list = self.construct_tags()
        activity = self.construct_activity(project)
        description = self.request.json.get('description', None)
        for te in time_entries:
            te.project = project
            te.activity = activity
            te.description = description
            te.calculate_working_hours_only = working_hours_only
            for tag in tag_list:
                te.add_tag(tag)
            self.timesheet.add(te)
            entries.append(te.to_dict())

        return {
            'type' : 'timelog',
            'result' : entries
            }

    @cornice_view(validators=(validators.strict_url_timeentry_id_exists,))
    def delete(self):
        """DELETE shouldn't contain body so we can't send the userid of a
        time-entry.

        WSGIWarning: You are not supposed to send a body in a DELETE
        request. Most web servers will ignore it
        """
        tl = self.timesheet.get_all_timelog()
        old_te = tl.get_ute(self.request.matchdict['timeentry_id'])

        perm = 'timesheet.manage'
        if old_te.user != self.current_user:
            perm = 'timesheet.manage.others'
        if not self.has_context_permission(perm, old_te.project):
            return

        self.timesheet.remove(old_te)

    @cornice_view(schema=CollectionGetSchema,
                  validators=(users.validators.qs_userid_exists,))
    def collection_get(self):
        """GET method on collection without any query string parameters will
        give first page listing of timelog entries made by the
        authenticated user, with the default page_size being 10.

        Apart from this behaviour, an user can specify the page,
        page_size and the userid for which he/she wants the timelog
        entries. If the authenticated user is requesting for entries
        of another user appropriate permissions are validated before
        giving out the results.

        The time entries will be in reverse datetime order. Along with
        the timelog entries, current_page, page_size and total_pages
        available for the current page_size information are
        provided. The number of timelog entries will be less than or
        equal to the given page_size.
        """
        userid = self.request.params.get("userid", None)
        if userid:
            user = self.users.get(userid)
        else:
            user = self.current_user

        page = self.request.validated['page']
        page_size = self.request.validated["page_size"]
        if user != self.current_user and not self.has_context_permission(
                'timesheet.view', self.request.root):
            # The given user is not current user, so we check whether
            # the user has 'timesheet.view' permission in root. If he
            # doesn't have it in root, we check for individual
            # projects of the given user
            projects = []
            for p in user.projects:
                if self.has_context_permission('timesheet.view', p):
                    projects.append(p)
            if user.projects and not projects:
                # The current user doesn't have view permission on any
                # of the given user's projects
                self.request.errors.add(
                    'querystring', 'userid'
                    "User='%s' has no permission to access data for User='%s'" %
                    (self.current_user.userid, user.userid))
                self.request.errors.status = HTTPForbidden.code
                return
            ts = self.timesheet.get_user_timesheet(user)
            # construct tl with union of all keys
            oo_set = OOSet()
            for p in projects:
                oo_set = union(oo_set, ts.get_project(p)._ite_logs)
            container = PaginatedTimelogContainer(
                ts.get_all(), oo_set, page_size)
        else:
            tl = self.timesheet.get_user_timelog(user)
            container = PaginatedContainer(
                tl.get_ite_entries(None, None), page_size)

        # ensure page-number is valid
        if container.is_valid(page) is False:
            self.request.errors.add(
                'querystring', 'page', 'Page number (%d) out of range' % page)
            self.request.errors.status = HTTPBadRequest.code
            return

        # fetch page and render
        entries = container.get(page)
        timelog = []
        for entry in entries:
            timelog.append(entry.to_dict())

        return {
            'type' : 'timelog',
            'current_page' : page,
            'page_size' : page_size,
            'total_pages' : container.page_count,
            'total_size' : container.size,
            'result' : timelog
        }

    @cornice_view(schema=AddTimeEntriesSchema,
                  validators=(users.validators.body_userid_exists,
                              projects.validators.body_project_id_exists))
    def collection_post(self):
        # post will contain a set of time-entry
        userid = self.request.params.get("userid", None)
        if userid:
            user = self.users.get(userid)
        else:
            user = self.current_user

        project = self.projects.get(self.request.json.get('project_id'))

        perm = 'timesheet.manage'
        if user != self.current_user:
            perm = 'timesheet.manage.others'
        if not self.has_context_permission(perm, project):
            return

        ignore_conflicts = self.request.validated['ignore_conflicts']
        working_hours_only = self.request.validated['working_hours_only']
        time_entries = []
        for ts in self.request.json['time_splits']:
            try:
                te = UITimeEntry.from_dict(ts)
            except errors.BadParameter, e:
                self.request.errors.add('body', 'start_time', str(e))
                self.request.errors.status = HTTPBadRequest.code
                return
            te.user = user
            if ignore_conflicts is False:
                status, entries = self.timesheet.calculate_fit(te)
                if status != "good":
                    self.request.errors.add(
                        'body', 'time_splits',
                        "Conflicting entry in time splits found")
                    self.request.errors.status = HTTPBadRequest.code
                    return
            time_entries.append(te)

        entries = []
        tag_list = self.construct_tags()
        activity = self.construct_activity(project)
        description = self.request.json.get('description', None)
        for te in time_entries:
            te.project = project
            te.activity = activity
            te.description = description
            te.calculate_working_hours_only = working_hours_only
            for tag in tag_list:
                te.add_tag(tag)
            self.timesheet.add(te)
            entries.append(te.to_dict())

        return {
            'type' : 'timelog',
            'result' : entries
            }

service_action_split = Service(
    name="api.timesheet.split", path="/api/v1/timesheet/action/split",
    permission="timesheet.view")

@service_action_split.post(schema=SplitActionSchema,
                           validators=(users.validators.body_userid_exists,))
def get_split_info(request):
    users = request.root['users']
    timesheet = request.root['timesheet']
    current_user = users.get(authenticated_userid(request))
    user = users.get(request.json.get("userid", current_user.userid))

    # Ensure only admin users can add get split-up for others
    if user != current_user and not current_user.is_admin():
        request.errors.add(
            'body', 'userid',
            'User=%s cannot get split info for user=%s' % \
                (current_user.userid, user.userid))
        request.errors.status = HTTPForbidden.code
        return

    exclude = request.json.get("exclude", None)
    if exclude is not None:
        if timesheet.get_user_timelog(user).get_ute(exclude, None) is None:
            request.errors.add(
                'body', 'exclude',
                "Invalid exclusion timeentry_id='%s'" % exclude)
            request.errors.status = HTTPBadRequest.code
            return

    try:
        te = UITimeEntry.from_dict(request.json)
    except errors.BadParameter, e:
        request.errors.add('body', 'start_time', str(e))
        request.errors.status = HTTPBadRequest.code
        return

    te = UITimeEntry.from_dict(request.json)
    te.user = user
    status, entry = timesheet.calculate_fit(te, exclude)
    data = {
        'type' : 'timesheet-split',
        'result' : [],
        'status' : status
    }
    if entry is not None:
        for e in entry:
            data['result'].append(e.to_dict())
    return data

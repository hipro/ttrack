# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    )

def url_timeentry_id_exists(request):
    tl = request.root['timesheet'].get_all_timelog()
    timeentry_id = request.matchdict['timeentry_id']
    te = tl.get_ute(timeentry_id, None)
    if te is None:
        request.errors.add(
            'url', 'timeentry_id',
            "TimeEntry='%s' does not exist" % (timeentry_id,))
        request.errors.status = HTTPNotFound.code

def strict_url_timeentry_id_exists(request):
    tl = request.root['timesheet'].get_all_timelog()
    timeentry_id = request.matchdict['timeentry_id']
    te = tl.get_ute(timeentry_id, None)
    if te is None:
        request.errors.add(
            'url', 'timeentry_id',
            "TimeEntry='%s' does not exist" % (timeentry_id,))
        request.errors.status = HTTPBadRequest.code

# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

class TTrackError(Exception):
    pass

class BadParameter(TTrackError):
    pass

class BadOperation(TTrackError):
    pass

class AlreadyExists(TTrackError):
    pass

class DoesNotExist(TTrackError):
    pass

class InvalidEmailAddress(TTrackError):
    pass

class SimpleFormError(TTrackError):

    def __init__(self, form_errors):
        if isinstance(form_errors, dict):
            str_errors = ['%s: %s' % (k, form_errors[k]) for k in form_errors]
            self.msg = '; '.join(str_errors)
        else:
            self.msg = form_errors

    def __str__(self):
        return repr(self.msg)

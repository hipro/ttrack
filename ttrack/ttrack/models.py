# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

from persistent import Persistent
from persistent.mapping import PersistentMapping
from zope import interface
from zope import event
from repoze.folder import Folder

import users
import projects
import clients
import timesheet
import tags
from ttrack.interfaces import (
    IApplicationCreatedEvent,
    IApplicationWillBeCreatedEvent
)

@interface.implementer(IApplicationWillBeCreatedEvent)
class ApplicationWillBeCreatedEvent(object):

    def __init__(self, app):
        self.app = app

@interface.implementer(IApplicationCreatedEvent)
class ApplicationCreatedEvent(object):

    def __init__(self, app):
        self.app = app

class AppRoot(Folder):
    __acl__ = None

    def set_acl(self, acl):
        self.__acl__ = acl

def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        zodb_root['app_root'] = create_app_root()
        # Raise an event so that initial data can be populated
        event.notify(ApplicationCreatedEvent(zodb_root['app_root']))
        import transaction
        transaction.commit()
    return zodb_root['app_root']

def create_app_root():
    app_root = AppRoot()
    # Raise an event so that all modules can initialize themselves
    event.notify(ApplicationWillBeCreatedEvent(app_root))
    return app_root

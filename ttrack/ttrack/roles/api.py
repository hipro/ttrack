# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

from pyramid.httpexceptions import HTTPBadRequest

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )
import colander
from zope import event

from . import validators
from .models import Role, PERMISSIONS, RoleModifiedEvent

class Permissions(colander.SequenceSchema):
    permissions = colander.SchemaNode(
        colander.String(), location="body", type="str")

class RoleSchema(colander.MappingSchema):
    role_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")
    permissions = Permissions(validator=colander.Length(min=1))

@cornice_resource(
    name="api.roles",
    collection_path="/api/v1/roles",
    path="/api/v1/roles/{role_id}.json",
    permission="roles.manage",
)
class API(object):

    def __init__(self, request):
        self.request = request
        self.roles = request.root['roles']

    def is_role_id_unique(self, request):
        role_id = self.request.json.get('role_id', None)
        if role_id and not self.roles.is_id_unique(role_id):
            self.request.errors.add('body', 'role_id', 'Role already exists')
            self.request.errors.status = HTTPBadRequest.code

    def is_url_user_role(self, request):
        role_id = self.request.matchdict['role_id']
        role = self.roles.get(role_id, None)

    def is_permissions_valid(self, request):
        permissions = self.request.json.get('permissions', None)
        if permissions is None:
            return
        for permission in permissions:
            if permission not in PERMISSIONS:
                self.request.errors.add(
                    'body', 'permissions', "Invalid permission (%s)" %
                    permission)
                self.request.errors.status = HTTPBadRequest.code
                return

    @cornice_view(permission="roles.view")
    def collection_get(self):
        return {
            'type' : 'role_list',
            'result' : [role.to_dict() for role in self.roles.values()]
        }

    @cornice_view(schema=RoleSchema,
                  validators=('is_role_id_unique','is_permissions_valid',))
    def collection_post(self):
        role = Role.from_dict(self.request.json)
        self.roles.add(role)
        return {
            'type': 'role',
            'result' : role.to_dict()
        }

    @cornice_view(permission="roles.view",
                  validators=(validators.url_role_id_exists,))
    def get(self):
        role_id = self.request.matchdict['role_id']
        role = self.roles.get(role_id)
        return {
            'type' : 'role',
            'result' : role.to_dict()
        }

    @cornice_view(schema=RoleSchema,
                  validators=(validators.url_role_id_exists,
                              'is_permissions_valid', 'is_url_user_role'))
    def put(self):
        role_id = self.request.matchdict['role_id']

        # enforce 'role_id' is read-only
        if role_id != self.request.json_body['role_id']:
            self.request.errors.add(
                'body', 'role_id', 'role_id is read-only')
            self.request.errors.status = HTTPBadRequest.code
            return

        role = self.roles.get(role_id)
        updated_role = self.request.json

        role.name = updated_role['name']

        to_remove = []
        p_modified = False
        for permission in role.permissions:
            if permission not in updated_role['permissions']:
                p_modified = True
                to_remove.append(permission)

        for permission in to_remove:
            role.remove_permission(permission)

        for permission in updated_role['permissions']:
            if permission not in role.permissions:
                p_modified = True
                role.add_permission(permission)

        if p_modified is True:
            # We fire RoleModifiedEvent only if there is a change in
            # permissions. We are not bothered about name change alone
            event.notify(RoleModifiedEvent(role))

        return {
            'type' : 'role',
            'result' : role.to_dict()
        }

    @cornice_view(validators=(validators.url_role_id_exists,'is_url_user_role'))
    def delete(self):
        role_id = self.request.matchdict['role_id']
        self.roles.remove(role_id)

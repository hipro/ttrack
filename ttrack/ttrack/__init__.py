# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

# We need to monkey-patch cornice to integrate properly with
# colander. So, do this first.
import custom_cornice
custom_cornice.install()

# This is necessary for zope events to work.
import zope.component.event

import os

from zope.component import getGlobalSiteManager

from pyramid.config import Configurator
from pyramid_zodbconn import get_connection
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import (
    Authenticated,
    NO_PERMISSION_REQUIRED
    )
from pyramid.static import static_view

import users
from .models import appmaker
from .handlers import register_zca_subscribers

def root_factory(request):
    conn = get_connection(request)
    return appmaker(conn.root())

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    authn_policy = AuthTktAuthenticationPolicy(
        'sosecret', callback=users.find_group, hashalg='sha512')
    authz_policy = ACLAuthorizationPolicy()

    registry = getGlobalSiteManager()
    config = Configurator(registry=registry)
    config.setup_registry(root_factory=root_factory, settings=settings)
    config.load_zcml('ttrack:configure.zcml')
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.set_default_permission("view")

    pub_dir = settings['ttrack.publish_folder']
    config.add_static_view(name="app", path=os.path.join(pub_dir, "app"))
    config.add_static_view(name="scripts", path=os.path.join(pub_dir, "scripts"))
    config.add_static_view(name="styles", path=os.path.join(pub_dir, "styles"))
    config.add_static_view(name="fonts", path=os.path.join(pub_dir, "fonts"))
    config.add_static_view(name="vendor", path=os.path.join(pub_dir, "vendor"))

    config.add_route("home", "/")
    config.add_route("login", "/login")
    config.add_route("logout", "/logout")
    config.add_route("csv_user", "/csv_user/{userid}")
    config.add_route("csv_project", "/csv_project/{project_id}")
    config.add_route("csv_client", "/csv_client/{client_id}")
    config.add_route("csv_tag", "/csv_tag/{tag_name}")

    config.scan()
    return config.make_wsgi_app()

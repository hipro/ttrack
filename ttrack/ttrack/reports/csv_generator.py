# Copyright (C) 2013,2014 HiPro IT Solutions Private Limited. All
# rights reserved.
#
# This program and the accompanying materials are made available under
# the terms described in the LICENSE file which accompanies this
# distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
log = logging.getLogger(__name__)

import cStringIO
import csv, tempfile

from pyramid.security import authenticated_userid
from pyramid.httpexceptions import HTTPBadRequest
from pyramid.view import view_config, view_defaults
from pyramid.response import Response, FileIter

from ttrack import users, projects, tags, clients


class CSVExportBase(object):

    def __init__(self, request):
        self.request = request
        self.clients = request.root['clients']
        self.users = request.root['users']
        self.tags = request.root['tags']
        self.projects = request.root['projects']
        self.timesheet = request.root['timesheet']
        self.current_user = self.users[authenticated_userid(request)]
        self.start_date = None
        self.end_date = None

    def _url_get_user(self):
        return self.users.get(self.request.matchdict['userid'])

    def _url_get_client(self):
        return self.clients.get(self.request.matchdict['client_id'])

    def _url_get_project(self):
        return self.projects.get(self.request.matchdict['project_id'])

    def _url_get_tag(self):
        return self.tags.get(self.request.matchdict['tag_name'])


    def _qs_get_user(self):
        userid = self.request.params.get('userid', None)
        if userid:
            return self.users.get(userid)
        return None

    def _qs_get_client(self):
        client_id = self.request.params.get('client_id', None)
        if client_id:
            return self.clients.get(client_id)
        return None

    def _qs_get_project(self):
        project_id = self.request.params.get('project_id', None)
        if project_id:
            return self.projects.get(project_id)
        return None

    def _qs_get_activity(self, project):
        activity_name = self.request.params.get('activity', None)
        if activity_name:
            return project.get_activity(activity_name)
        return None

    def _qs_get_tag(self):
        tag_name = self.request.params.get('tag_name', None)
        if tag_name:
            return self.tags.get(tag_name)
        return None

    def _validate_and_extract_dates(self):
        start_date = self.request.params.get("start_date", None)
        if start_date:
            try:
                self.start_date = datetime.datetime.strptime(
                    start_date, "%Y-%m-%d")
            except ValueError:
                raise HTTPBadRequest("start_date='%s' invalid" % start_date)
        end_date = self.request.params.get("end_date", None)
        if end_date:
            td = datetime.timedelta(hours=23, minutes=59, seconds=59)
            try:
                self.end_date = datetime.datetime.strptime(
                    end_date, "%Y-%m-%d") + td
            except ValueError:
                raise HTTPBadRequest("end_date='%s' invalid" % end_date)

    def _create_tmp_csv_file(self, entries):
        settings = self.request.registry.settings
        tmp_dir = settings['ttrack.temp_folder']
        tmp_file = tempfile.TemporaryFile(dir=tmp_dir)
        writer = csv.DictWriter(
            tmp_file,
            ('date', 'start_time', 'end_time', 'userid', 'user_name',
             'project_id', 'project_name', 'client_id', 'client_name',
             'activity', 'tags', 'description', 'duration'),
            extrasaction='ignore')
        total_mins = 0
        for ent in entries:
            row = ent.to_dict()
            total_mins += row['duration']
            row['duration'] = get_duration_string(row['duration'])
            writer.writerow(row)

        if total_mins > 0:
            row = { 'description' : 'Total duration',
                    'duration' : get_duration_string(total_mins) }
            writer.writerow(row)

        # Move the file pointer to start
        tmp_file.seek(0)
        return tmp_file

def get_duration_string(d):
    """Given total duration in minutes split them into hh:mm string"""
    h = d // 60.0
    m = d - (h * 60)
    return "%02d:%02d" % (h, m)


@view_config(permission="reports.view", route_name="csv_user")
class UserCSVExportView(CSVExportBase):

    def __call__(self):
        # validators
        users.validators.pv_url_userid_exists(self.request)
        clients.validators.pv_qs_client_id_exists(self.request)
        projects.validators.pv_qs_project_id_exists(self.request)
        tags.validators.pv_qs_tag_name_exists(self.request)

        # validate and create datetime objects
        self._validate_and_extract_dates()

        user = self._url_get_user()            # mandatory
        client = self._qs_get_client()         # optional
        project = self._qs_get_project()       # optional
        tag = self._qs_get_tag()               # optional

        timelog = self.timesheet.get_user_filtered_timelog(
            user, client, project, tag)
        entries = timelog.get_ite_entries(self.start_date, self.end_date)

        # Create temporary CSV file
        csv_file = self._create_tmp_csv_file(entries)

        # Construct the response
        res = self.request.response
        res.content_type = "text/csv; name=user_report.csv"
        res.content_disposition = "attachment; filename=user_report.csv"
        res.app_iter = FileIter(csv_file)
        return res

@view_config(permission="reports.view", route_name="csv_project")
class ProjectCSVExportView(CSVExportBase):

    def _validate_activity_name(self):
        project_id = self.request.matchdict['project_id']
        project = self.projects.get(project_id, None)
        if project is None:
            # We dont care about non-existant project_id. That is
            # someone else's problem.
            return
        activity_name = self.request.params.get('activity', None)
        if activity_name is None:
            return
        activity = project.get_activity(activity_name, None)
        if activity is None:
            raise HTTPBadRequest("Activity='%s' does not exist" % activity_name)


    def __call__(self):
        # validators
        projects.validators.pv_url_project_id_exists(self.request)
        users.validators.pv_qs_userid_exists(self.request)
        tags.validators.pv_qs_tag_name_exists(self.request)

        self._validate_activity_name()
        # validate and create datetime objects
        self._validate_and_extract_dates()

        project = self._url_get_project()          # mandatory
        activity = self._qs_get_activity(project)  # optional
        user = self._qs_get_user()                 # optional
        tag = self._qs_get_tag()                   # optional

        timelog = self.timesheet.get_project_filtered_timelog(
            project, user, activity, tag)
        entries = timelog.get_ite_entries(self.start_date, self.end_date)

        # Create temporary CSV file
        csv_file = self._create_tmp_csv_file(entries)

        # Construct the response
        res = self.request.response
        res.content_type = "text/csv; name=project_report.csv"
        res.content_disposition = "attachment; filename=project_report.csv"
        res.app_iter = FileIter(csv_file)
        return res

@view_config(permission="reports.view", route_name="csv_client")
class ClientCSVExportView(CSVExportBase):

    def __call__(self):
        # validators
        clients.validators.pv_url_client_id_exists(self.request)
        projects.validators.pv_qs_project_id_exists(self.request)
        users.validators.pv_qs_userid_exists(self.request)
        tags.validators.pv_qs_tag_name_exists(self.request)

        # validate and create datetime objects
        self._validate_and_extract_dates()

        client = self._url_get_client()           # mandatory
        project = self._qs_get_project()          # optional
        user = self._qs_get_user()                # optional
        tag = self._qs_get_tag()                  # optional

        timelog = self.timesheet.get_client_filtered_timelog(
            client, project, user, tag)
        entries = timelog.get_ite_entries(self.start_date, self.end_date)

        # Create temporary CSV file
        csv_file = self._create_tmp_csv_file(entries)

        # Construct the response
        res = self.request.response
        res.content_type = "text/csv; name=client_report.csv"
        res.content_disposition = "attachment; filename=client_report.csv"
        res.app_iter = FileIter(csv_file)
        return res


@view_config(permission="reports.view", route_name="csv_tag")
class TagCSVExportView(CSVExportBase):

    def __call__(self):
        # validators
        tags.validators.pv_url_tag_name_exists(self.request)
        users.validators.pv_qs_userid_exists(self.request)
        clients.validators.pv_qs_client_id_exists(self.request)
        projects.validators.pv_qs_project_id_exists(self.request)

        # validate and create datetime objects
        self._validate_and_extract_dates()

        tag = self._url_get_tag()                 # mandatory
        user = self._qs_get_user()                # optional
        client = self._qs_get_client()            # optional
        project = self._qs_get_project()          # optional

        timelog = self.timesheet.get_tag_filtered_timelog(
            tag, user, client, project)
        entries = timelog.get_ite_entries(self.start_date, self.end_date)

        # Create temporary CSV file
        csv_file = self._create_tmp_csv_file(entries)

        # Construct the response
        res = self.request.response
        res.content_type = "text/csv; name=tag_report.csv"
        res.content_disposition = "attachment; filename=tag_report.csv"
        res.app_iter = FileIter(csv_file)
        return res


# class CSVIter(object):

#     def __init__(self, entries):
#         self.entries = entries
#         self.index = 0

#     def __iter__(self):
#         return self

#     def next(self):
#         if self.index >= len(self.entries):
#             raise StopIteration
#         ent = self.entries[self.index]
#         out = cStringIO.StringIO()
#         writer = csv.DictWriter(
#             out, ('date', 'start_time', 'end_time', 'userid', 'user_name',
#                   'project_id', 'project_name', 'client_id', 'client_name',
#                   'activity', 'tags', 'description'), extrasaction='ignore')
#         writer.writerow(ent.to_dict())
#         self.index += 1
#         return out.getvalue()

#     __next__ = next # py3

#     def close(self):
#         pass

angular.module('ttrack.users.controllers.profile.password', [
  'ui.router',
  'ui.utils',
  'ttrack.resources',
  'ttrack.utils',
  'ttrack.users.directives'
]).controller(
  'editProfilePassword',
  ['$scope', 'UserProfile', 'apiErrorDialog',
   function ($scope, UserProfile, errorDlg) {
     $scope.alert = { show: false,
                      type: '',
                      msg: ''};

     $scope.closeAlert = function () {
       $scope.alert.show = false;
       $scope.alert.type = '';
       $scope.alert.msg = '';
     };

     function update () {
       UserProfile.get(
         function(data) {
           $scope.user = data.result;
         },
         function(err_res) {
           console.log("Unable to get user profile");
         });
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.changePasswordForm.$dirty && $scope.changePasswordForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.changePasswordForm.$valid) {
         return;
       }
       UserProfile.save(
         $scope.user,
         function (data) {
           $scope.alert.show = true;
           $scope.alert.type = 'success';
           $scope.alert.msg = 'Password changed sucessfully.';
           $scope.changePasswordForm.$setPristine(true);
           $scope.user.old_password = $scope.confirm_password = "";
           update();
         },
         function (err_res) {
           $scope.alert.show = true;
           $scope.alert.type = 'danger';
           $scope.alert.msg = err_res.data.errors[0].description;
           $scope.changePasswordForm.$setPristine(true);
         });
     };
     update();
   }
  ]);

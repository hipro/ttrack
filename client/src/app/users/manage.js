angular.module('ttrack.users.manage', [
  'ui.router',
  'ttrack.users.controllers.list',
  'ttrack.users.controllers.add',
  'ttrack.users.controllers.show',
  'ttrack.users.controllers.edit'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('manage.users', {
        url: '/users',
        abstract: true,
        template: '<ui-view/>'
      })
      // Make this state as default by making the url as '' and its
      // parent as abstract
      .state('manage.users.list', {
        url: '',
        controller: 'userListCtrl',
        templateUrl: 'users/userList.tpl.html'
      })
      .state('manage.users.show', {
        url: '/show/:userId',
        controller: 'userShowCtrl',
        templateUrl: 'users/userShow.tpl.html'
      })
      .state('manage.users.add', {
        url: '/add',
        controller: 'userAddCtrl',
        templateUrl: 'users/userAdd.tpl.html'
      })
      .state('manage.users.edit', {
        url: '/edit/:userId',
        controller: 'userEditCtrl',
        templateUrl: 'users/userEdit.tpl.html'
      });


    }]);

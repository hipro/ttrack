angular.module('ttrack.users.directives', [
  'ttrack.resources'
])

  .directive(
    'uniqueUserid',
    ['User', function(User) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                User.get(
                  { id: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity(
                      'uniqueUserid', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity(
                      'uniqueUserid', true);
                  });
              }
            });
          });
        }
      };
    }])

  .directive(
    'uniqueEmail',
    ['UserCollection', function(UserCollection) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                UserCollection.query(
                  { email_id: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity(
                      'uniqueEmail', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity(
                      'uniqueEmail', true);
                  });
              }
            });
          });
        }
      };
    }])

  .directive(
    'uniqueEmailExclude',
    ['UserCollection', '$parse', function(UserCollection, $parse) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {

          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              var modelGetter = $parse(attrs.uniqueEmailExclude);
              var user_id = modelGetter(scope);
              if (viewValue && user_id) {
                UserCollection.query(
                  { email_id : viewValue },
                  function (data) {
                    if (data.result.userid ===
                        user_id) {
                      ngModelCtrl.$setValidity(
                        'uniqueEmailExclude',
                        true);
                    } else {
                      ngModelCtrl.$setValidity(
                        'uniqueEmailExclude',
                        false);
                    }
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity(
                      'uniqueEmailExclude',
                      true);
                  });
              }
            });
          });
        }
      };
    }]);

angular.module('ttrack.users.profile', [
  'ui.router',
  'ttrack.users.controllers.profile.edit',
  'ttrack.users.controllers.profile.password'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('profile', {
          parent: 'two-part-page',
          url: '/profile',
          views: {
            "sidebar": {
              controller: 'profilePageCtrl',
              templateUrl: 'users/profilePageLinks.tpl.html'
            },
            "": {
              controller: 'profilePageCtrl',
              // Switching state from template is an hack. Even though
              // the 'profilePageCtrl' specified in 'sidebar' view
              // should switch the state automatically but I found
              // that this doesn't happen consistently so this
              // template gets rendered at times (which should not
              // happen). So, I forcefully call the switchState
              // function from the template code.
              template: "<div data-ui-view>Profile {{ switchState() }}</div>"
            }
          }
        })
        .state('profile.edit', {
          url: "/edit_details",
          controller: 'editProfileDetails',
          templateUrl: 'users/editProfile.tpl.html'
        })
        .state('profile.password', {
          url: "/change_password",
          controller: 'editProfilePassword',
          templateUrl: 'users/changePassword.tpl.html'
        });


    }])

  .controller(
    "profilePageCtrl", ['$scope', '$state', function($scope, $state) {

      $scope.links = [
        {
          name: "Edit Details",
          sref: "profile.edit"
        },
        {
          name: "Change Password",
          sref: "profile.password"
        }
      ];

      $scope.switchState = function () {
        if ($state.is("profile")) {
          $state.go("profile.edit");
        }
      };
      $scope.switchState();

    }]);

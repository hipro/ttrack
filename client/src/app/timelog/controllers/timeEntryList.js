angular.module('ttrack.timelog.controllers.list', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  "timeEntryListCtrl",
  ['$scope', 'TimeSheet', 'confirmationDialogYesNo',
   'apiErrorDialog', 'TimeSheetCollection',
   function ($scope, TimeSheet, confirmDlg, errorDlg, TimeSheetCollection) {
     $scope.page_number = 1;
     $scope.pnMaxSize = 5;
     function update () {
       TimeSheetCollection.get (
         {
           page : $scope.page_number
         },
         function (data) {
           $scope.pnItemsPerPage = data.page_size;
           $scope.pnTotalItems = data.total_size;
           $scope.pnCurrentPage = data.current_page;
           $scope.timelogs = [];
           var date_wise = {date: '', logs: []};
           var first = true;
           for (var index in data.result) {
             var te = data.result[index];
             if (date_wise.date != te.date) {
               if (!first) {
                 $scope.timelogs.push(date_wise);
                 date_wise = {date: '', logs: []};
               }
               first = false;
               date_wise.date = te.date;
             }
             date_wise.logs.push(te);
           }
           if (date_wise.date) {
             $scope.timelogs.push(date_wise);
           }
         });
     }

     $scope.pageChanged = function (page) {
       $scope.page_number = page;
       update();
     };

     $scope.deleteTimeEntry = function (timeentry_id) {
       var result = confirmDlg.show (
         "Delete timelog entry?",
         "This will affect the total hours spent on the project. Do you " +
           "want to continue?");
       result.then(function () {
         TimeSheet.remove(
           {timeEntryId: timeentry_id},
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };

     $scope.getDate = function (date_str) {
       var dt = new Date(date_str);
       return dt.toDateString();
     };

     $scope.getShortDescription = function (description) {
       if (description) {
         return description.split('\n')[0];
       }
       return "";
     };

     update();
   }]);

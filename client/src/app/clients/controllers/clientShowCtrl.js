angular.module('ttrack.clients.controllers.show', [
  'ngSanitize',
  'ttrack.resources'
]).controller(
  'clientShowCtrl',
  ['$scope', 'Client', '$stateParams', '$state',
   function ($scope, Client, $stateParams, $state) {
     var client_id = $stateParams.clientId;
     Client.get(
       {id: client_id},
       function(data) {
         $scope.client = data.result;
         $scope.html_address = $scope.client.address.replace(/\n/g, '<br />');
       },
       function(err_res) {
         console.log("Invalid client_id : " + client_id);
         $state.go("manage.clients.list");
       }
     );
   }
  ]);

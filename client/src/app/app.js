// Specifiying the requirement for a module will create a new module
// with the given name overwritting an existing module (if any) with
// that name
angular.module('ttrack', [
  'ui.router',
  'ttrack.app.templates',
  'ttrack.resources',
  'ttrack.utils',
  'ttrack.handlers',
  'ttrack.timelog',
  'ttrack.manage',
  'ttrack.users.profile',
  'ttrack.reports',
  'ttrack.providers',
  'ttrack.dashboard'
]);

// Refering a module without the dependencies will fetch the module
// instance
angular.module('ttrack').config(
  ['$stateProvider', function ($stateProvider) {
     $stateProvider
       .state('page', {
         abstract: true,
         controller: 'basePageCtrl',
          resolve: {
            // A string value resolves to a service
            userInfo: 'userInfo',
            // We don't use the 'gotData' in the controller as this is
            // used to ensure that the userInfo gets its data filled
            // from the $resource
            gotData : ["userInfo", function(userInfo) {
                return userInfo.initializedData();
            }]
          },
         templateUrl: 'page.tpl.html'
       })
       .state('two-part-page', {
         parent: 'page',
         templateUrl: 'twoPartPage.tpl.html'
       });

   }])
  .run(
    ['$rootScope', '$state', '$stateParams',
     function ($rootScope,   $state,   $stateParams) {

       // It's very handy to add references to $state and $stateParams
       // to the $rootScope so that you can access them from any scope
       // within your applications.For example, <li
       // ui-sref-active="active }"> will set the <li> // to active
       // whenever 'contacts.list' or one of its decendents is active.
       $rootScope.$state = $state;
       $rootScope.$stateParams = $stateParams;
     }
    ]
  );

angular.module('ttrack').controller(
  "basePageCtrl",
  ['$scope', '$rootScope', '$state', 'userInfo',
   function($scope, $rootScope, $state, userInfo) {

     $scope.userInfo = userInfo;

     $scope.logout_url = $("#logout-url").text();
     $scope.home_url = $("#home-url").text();

     $rootScope.$on(
       '$stateChangeStart',
       function (event, toState, toParams, fromState, fromParams) {
         console.log(
           '$stateChangeStart to ' + toState.to +
             '- fired when the transition begins. ' +
             'toState,toParams : \n',toState, toParams);
       });

     $rootScope.$on(
       '$stateChangeError',
       function(event, toState, toParams, fromState, fromParams) {
         console.log('$stateChangeError - fired when an error ' +
                     'occurs during transition.');
         console.log(arguments);
       });

    $rootScope.$on(
      '$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams) {
        console.log('$stateChangeSuccess to ' + toState.name +
                    '- fired once the state transition is complete.');
      });

     $rootScope.$on(
       '$viewContentLoaded',
       function(event){
         console.log('$viewContentLoaded - fired after dom rendered',event);
       });

     $rootScope.$on(
       '$stateNotFound',
       function(event, unfoundState, fromState, fromParams) {
         console.log('$stateNotFound ' + unfoundState.to +
                     '  - fired when a state cannot be found by its name.');
         console.log(unfoundState, fromState, fromParams);
       });
   }]);

angular.module('ttrack.roles.manage', [
  'ui.router',
  'ttrack.roles.controllers.list',
  'ttrack.roles.controllers.add',
  'ttrack.roles.controllers.show',
  'ttrack.roles.controllers.edit'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('manage.roles', {
        url: '/roles',
        abstract: true,
        template: '<ui-view/>'
      })
      // Make this state as default by making the url as '' and its
      // parent as abstract
      .state('manage.roles.list', {
        url: '',
        controller: 'roleListCtrl',
        templateUrl: 'roles/roleList.tpl.html'
      })
      .state('manage.roles.add', {
        url: '/add',
        controller: 'roleAddCtrl',
        templateUrl: 'roles/roleAdd.tpl.html'
      })
      .state('manage.roles.show', {
        url: '/show/:roleId',
        controller: 'roleShowCtrl',
        templateUrl: 'roles/roleShow.tpl.html'
      })
      .state('manage.roles.edit', {
        url: '/edit/:roleId',
        controller: 'roleEditCtrl',
        templateUrl: 'roles/roleEdit.tpl.html'
      });

    }]);

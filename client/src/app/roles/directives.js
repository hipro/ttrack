angular.module('ttrack.roles.directives', [
  'ttrack.resources'
])

  .directive(
    'uniqueRoleId',
    ['Role',
     function(Role) {
       return {
         require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
           element.on('blur', function (event) {
             scope.$apply(function () {
               var viewValue = element.val();
               if (viewValue) {
                 Role.get(
                   { id: viewValue },
                   function (data) {
                     ngModelCtrl.$setValidity('uniqueRoleId', false);
                   },
                   function (err_res) {
                     ngModelCtrl.$setValidity('uniqueRoleId', true);
                   });
               }
             });
           });
         }
       };
     }]);

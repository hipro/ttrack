angular.module('ttrack.roles.controllers.show', [
  'ttrack.resources'
]).controller(
  'roleShowCtrl',
  ['$scope', 'Role', '$stateParams', '$state',
   function ($scope, Role, $stateParams, $state) {
     var role_id = $stateParams.roleId;
     Role.get(
       {id: role_id},
       function(data) {
         $scope.role = data.result;
       },
       function(err_res) {
         console.log("Invalid role_id : " + role_id);
         $state.go("manage.roles.list");
       });
   }]);

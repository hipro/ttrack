angular.module('ttrack.reports', [
  'ui.router',
  'ttrack.reports.controllers.user',
  'ttrack.reports.controllers.project',
  'ttrack.reports.controllers.client',
  'ttrack.reports.controllers.tag'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('reports', {
          parent: 'page',
          abstract: true,
          template: '<div data-ui-view></div>',
          url: '/reports'
        })
        .state('reports.user', {
          url: '/user',
          controller: 'userReportCtrl',
          templateUrl: 'reports/userReport.tpl.html'
        })
        .state('reports.project', {
          url: '/project',
          controller: 'projectReportCtrl',
          templateUrl: 'reports/projectReport.tpl.html'
        })
        .state('reports.client', {
          url: '/client',
          controller: 'clientReportCtrl',
          templateUrl: 'reports/clientReport.tpl.html'
        })
        .state('reports.tag', {
          url: '/tag',
          controller: 'tagReportCtrl',
          templateUrl: 'reports/tagReport.tpl.html'
        });

    }]);

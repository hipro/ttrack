angular.module('ttrack.reports.controllers.tag', [
  'ui.bootstrap',
  'tc.chartjs',
  'ttrack.resources',
  'ttrack.utils',
  'ttrack.reports.providers'
]).controller(
  'tagReportCtrl',
  ['$scope', 'TagReport', 'TagTimeSheetCollection',
   'TagCollection', 'UserCollection', 'ClientCollection', 'ProjectCollection',
   'apiErrorDialog', 'rptColorCodeTable', 'rptUtils',
   function ($scope, TagReport, TagTimeSheetCollection,
             TagCollection, UserCollection, ClientCollection,
             ProjectCollection, errorDlg, rptColorCodeTable, rptUtils) {
     // query Parameter to be used with TimeSheet API to get
     // paginated time entries
     var qParams = {
       page: 1,
       page_size: 10
     };
     var csv_base_url = $("#rpt-url-csv-tag").text();
     $scope.csv_url = "";

     $scope.getTimeString = function (minutes) {
       if (minutes < 60) {
         return minutes + " minutes";
       }
       return parseFloat(minutes/60).toFixed(2) + " hours";
     };

     function update_csv_url () {
       var csv_uri = new URI(csv_base_url);
       csv_uri.segment($scope.filter_tag.name);
       var params = {};
       if ($scope.user_selected) {
         params.userid = $scope.filter_user.userid;
       }
       if ($scope.project_selected) {
         params.project_id = $scope.filter_project.project_id;
       }
       if ($scope.client_selected) {
         params.client_id = $scope.filter_client.client_id;
       }
       csv_uri.search(params);
       $scope.csv_url = csv_uri.toString();
     }

     $scope.pnMaxSize = 5;
     $scope.report_duration = "";
     $scope.filter_advanced_mode = false;
     var end_date_modified = false;
     $scope.ranges = rptUtils.getSelectRangeOptions();
     $scope.filter_range = $scope.ranges[0];

     $scope.startDateChanged = function () {
       if ($scope.filter_start_date) {
         if (!end_date_modified) {
           $scope.filter_end_date = $scope.filter_start_date;
         }
       }
     };

     $scope.endDateChanged = function () {
       end_date_modified = true;
     };

     $scope.$watchCollection(
       '[filter_start_date, filter_end_date]',
       function (newValues) {
         if (newValues[0] && newValues[1]) {
           if (newValues[0] > newValues[1]) {
             $scope.showDateInvalidError = true;
             return;
           }
         }
         $scope.showDateInvalidError = false;
       });

     $scope.getTimeClass = function () {
       if ($scope.showDateInvalidError) {
         return 'has-error';
       }
       return "";
     };

     $scope.getDate = function (date_str) {
       if (date_str) {
         var dt = new Date(date_str);
         return dt.toDateString();
       }
       return "-";
     };

     $scope.getDateDuration = function () {
       if ($scope.filter_advanced_mode) {
         if ($scope.filter_start_date && $scope.filter_end_date &&
             !$scope.showDateInvalidError) {
           return $scope.getDate($scope.filter_start_date) +
             ' to ' +
             $scope.getDate($scope.filter_end_date);
         }
         return "";
       }
       return $scope.filter_range.label;
     };

     function getTag() {
       if ($scope.filter_tag && $scope.filter_tag.name) {
         return ": " + $scope.filter_tag.name;
       }
       return "";
     }

     $scope.canApply = function () {
       if ($scope.filter_tag && $scope.filter_tag.name) {
         // If we have a name property in filter_tag that means
         // that we have choosen the right tag from the
         // typeahead widget
         if (!$scope.filter_advanced_mode) {
           return true;
         }
         if ($scope.filter_start_date && $scope.filter_end_date &&
             !$scope.showDateInvalidError) {
           return true;
         }
       }
       return false;
     };

     $scope.cbOnUserSelect = function () {
       $scope.user_selected = true;
       update();
     };

     $scope.closeUserFilter = function () {
       $scope.user_selected = false;
       $scope.filter_user = '';
       update();
     };

     $scope.cbOnProjectSelect = function () {
       $scope.project_selected = true;
       if ($scope.client_selected) {
         if ($scope.filter_project.client_id != $scope.filter_client.client_id) {
           $scope.closeClientFilter();
           return;
         }
       }
       update();
     };

     $scope.closeProjectFilter = function () {
       $scope.project_selected = false;
       $scope.filter_project = '';
       update();
     };

     $scope.cbOnClientSelect = function () {
       $scope.client_selected = true;
       $scope.projects = $scope.filter_client.projects;
       if ($scope.project_selected) {
         if ($scope.filter_project.client_id != $scope.filter_client.client_id) {
           $scope.closeProjectFilter();
           return;
         }
       }
       update();
     };

     $scope.closeClientFilter = function () {
       $scope.client_selected = false;
       $scope.filter_client = '';
       $scope.projects = all_projects;
       update();
     };

     function constructGraphicalData(data) {
       var colorCode = rptColorCodeTable;
       $scope.projectChartData = [];
       $scope.userChartData = [];
       $scope.clientChartData = [];
       var chart_data;
       for (var d in data.projects) {
         chart_data = {};
         chart_data.value = data.projects[d].total;
         chart_data.color = colorCode.getColor();
         chart_data.label = d;
         chart_data.style = colorCode.getStyle();
         $scope.projectChartData.push(chart_data);
       }
       for (d in data.users) {
         chart_data = {};
         chart_data.value = data.users[d];
         chart_data.color = colorCode.getColor();
         chart_data.label = d;
         chart_data.style = colorCode.getStyle();
         $scope.userChartData.push(chart_data);
       }
       for (d in data.clients) {
         chart_data = {};
         chart_data.value = data.clients[d];
         chart_data.color = colorCode.getColor();
         chart_data.label = d;
         chart_data.style = colorCode.getStyle();
         $scope.clientChartData.push(chart_data);
       }
     }

     function clearSubFilters() {
       $scope.project_selected = false;
       $scope.filter_project = '';
       $scope.user_selected = false;
       $scope.filter_user = '';
       $scope.client_selected = false;
       $scope.filter_client = '';
     }

     $scope.applyFilter = function () {
       update_csv_url ();
       $scope.report_duration = $scope.getDateDuration();
       $scope.report_tag = getTag();
       $scope.timelogs = [];
       qParams.page = 1;
       $scope.show_graph_reports = false;
       $scope.show_log_reports = false;
       var params = {};
       if ($scope.filter_advanced_mode) {
         params.start_date = rptUtils.getDateString($scope.filter_start_date);
         params.end_date = rptUtils.getDateString($scope.filter_end_date);
       } else {
         params.start_date = $scope.filter_range.start_date;
         params.end_date = $scope.filter_range.end_date;
         params.time_key = $scope.filter_range.time_key;
       }
       params.tagName = $scope.filter_tag.name;

       TagReport.get(
         params,
         function (data) {
           $scope.tag_reports = data.result;
           $scope.records_available = false;
           $scope.filter_applied = true;
           if (data.result.total > 0) {
             $scope.show_graph_reports = true;
             $scope.records_available = true;
             constructGraphicalData(data.result);
             // Copy the start and end date to qParams in order to
             // query TimeSheet API for time-entries
             qParams.tagName = params.tagName;
             qParams.start_date = params.start_date;
             qParams.end_date = params.end_date;
             update();
           } else {
             clearSubFilters();
           }
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     var all_projects;
     ProjectCollection.get (
       function (data) {
         $scope.projects = data.result;
         all_projects = data.result;
       },
       function (err_res) {
         console.log("Error occurred while getting projects : " + err_res);
       });

     TagCollection.get (
       function (data) {
         $scope.tags = data.result;
       },
       function (err_res) {
         console.log("Error occurred while gettings tags : " + err_res);
       });

     UserCollection.get (
       function (data) {
         $scope.users = data.result;
       },
       function (err_res) {
         console.log("Error occurred while getting users : " + err_res);
       });

     ClientCollection.get (
       function (data) {
         $scope.clients = data.result;
       },
       function (err_res) {
         console.log("Error occurred while getting clients : " + err_res);
       });

     function get_params () {
       var params = {};
       params.page = qParams.page;
       params.page_size = qParams.page_size;
       params.tagName = qParams.tagName;
       params.start_date = qParams.start_date;
       params.end_date = qParams.end_date;
       if ($scope.user_selected) {
         params.userid = $scope.filter_user.userid;
       } else {
         params.userid = null;
       }
       if ($scope.client_selected) {
         params.client_id = $scope.filter_client.client_id;
       } else {
         params.client_id = null;
       }
       if ($scope.project_selected) {
         params.project_id = $scope.filter_project.project_id;
       } else {
         params.project_id = null;
       }
       return params;
     }

     function update() {
       update_csv_url ();
       var params = get_params();
       TagTimeSheetCollection.get (
         params,
         function (data) {
           $scope.show_log_reports = true;
           $scope.pnItemsPerPage = data.page_size;
           $scope.pnTotalItems = data.total_size;
           $scope.pnCurrentPage = data.current_page;
           $scope.timelogs = [];
           var date_wise = {date: '', logs: []};
           var first = true;
           for (var index in data.result) {
             var te = data.result[index];
             if (date_wise.date != te.date) {
               if (!first) {
                 $scope.timelogs.push(date_wise);
                 date_wise = {date: '', logs: []};
               }
               first = false;
               date_wise.date = te.date;
             }
             date_wise.logs.push(te);
           }
           if (date_wise.date) {
             $scope.timelogs.push(date_wise);
           }
         });
     }

     $scope.pageChanged = function (page) {
       qParams.page = page;
       update();
     };

     $scope.getShortDescription = function (description) {
       if (description) {
         return description.split('\n')[0];
       }
       return "";
     };
   }]);

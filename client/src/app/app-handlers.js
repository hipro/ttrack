angular.module('ttrack.handlers', [
  'ttrack.utils'
])
  .config(
    [ '$httpProvider', '$provide', function ($httpProvider, $provide) {
      $httpProvider.interceptors.push('ttrackHttpInterceptor');

      $httpProvider.defaults.transformRequest.push(
        function (data, headersGetter) {
          $("#app-loading").show();
          return data;
        });

      // Hook to default exception handler of AngularJS
      $provide.decorator(
        "$exceptionHandler", ['$delegate', function ($delegate) {
          return function (exception, cause) {
            $("#app-error").show().text(
              "Error occurred! " + exception.message + " : " + cause);
            $delegate(exception, cause);
          };
        }]);

    }])

  .factory(
    'ttrackHttpInterceptor',
    [ '$q', function ($q) {
      function hide_loading () {
        $("#app-loading").hide().parent().css("margin", "0px");
      }

      function hide_error () {
        $("#app-error").hide();
      }

      function show_error (msg) {
        $("#app-error").show().text(msg);
      }

      return {
        'requestError': function (rejection) {
          hide_loading();
          show_error("Error: Got bad HTTP request (" +
                     rejection.statusText + ")");
          return $q.reject(rejection);
        },
        'response': function (response) {
          hide_loading();
          hide_error();
          return response;
        },
        'responseError': function (err) {
          hide_loading();
          if (err.status === 0) {
            show_error("Error: Unable to establish HTTP connection");
          } else if (err.status != 404) {
            show_error("Error: Got bad HTTP response (" + err.statusText + ")");
          }
          return $q.reject(err);
        }
      };
    }]);

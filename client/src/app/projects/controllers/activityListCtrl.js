angular.module('ttrack.projects.controllers.activities.list', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'activityListCtrl',
  ['$scope', 'ActivityCollection', 'Activity',
   'confirmationDialogYesNo', 'apiErrorDialog', '$stateParams', '$state',
   function ($scope, ActivityCollection, Activity, confirmDlg,
             errorDlg, $stateParams, $state) {
     $scope.project_id = $stateParams.projectId;

     function update () {
       ActivityCollection.get(
         {projectId: $scope.project_id},
         function (data) {
           $scope.activities = data.result;
         },
         function (err_res) {
           console.log(
             "Unable to get activities for project : " + $scope.project_id);
           $state.go("manage.projects.show", {projectId: $scope.project_id});
         });
     }

     $scope.deleteActivity = function(act_name) {
       var result = confirmDlg.show(
         "Delete activity?",
         "All the timelog entries made for the activity '" + act_name + "' " +
           "will also be deleted. Do you want to continue?");
       result.then(function () {
         Activity.remove(
           {
             projectId: $scope.project_id,
             activityName: act_name
           },
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }
  ]);

angular.module('ttrack.projects.controllers.add', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.projects.directives'
]).controller(
  'projectAddCtrl',
  ['$scope', 'ProjectCollection', '$state', 'apiErrorDialog',
   'ClientCollection',
   function ($scope, ProjectCollection, $state, errorDlg, ClientCollection) {

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     ClientCollection.get(
       function (data) {
         $scope.clients = data.result;
       },
       function (err_res) {
         console.log("Error occurred while getting clients : " + err_res);
       });

     $scope.client = null;

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.save = function () {
       if (!$scope.projectAddForm.$valid) {
         return;
       }
       // Explicitly set the client_id in the project model as we
       // are maintaining the client information separately in
       // $scope.client
       $scope.project.client_id = $scope.client.client_id;

       ProjectCollection.save(
         $scope.project,
         function (data) {
           $state.go("manage.projects.list");
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return $scope.projectAddForm.$dirty && $scope.projectAddForm.$valid;
     };

   }
  ]);

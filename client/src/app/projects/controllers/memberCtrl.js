angular.module('ttrack.projects.controllers.members', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'memberCtrl',
  ['$scope', 'MemberCollection', 'Member', 'UserCollection',
   'confirmationDialogYesNo', 'apiErrorDialog', '$stateParams', '$state',
   function ($scope, MemberCollection, Member, UserCollection,
             confirmDlg, errorDlg, $stateParams, $state) {
     $scope.project_id = $stateParams.projectId;
     var all_users = null;
     UserCollection.get(
       function (data) {
         all_users = data.result;
         update();
       },
       function (err_res) {
         console.log("Unable to get user collection");
       });

     function get_user_diff(users, members) {
       var member_id={}, diff=[];
       for (var index in members) {
         var member = members[index];
         member_id[member.userid] = true;
       }
       for (var i in users) {
         var user = users[i];
         if (!member_id[user.userid]) {
           diff.push(user);
         }
       }
       return diff;
     }

     function update () {
       MemberCollection.get(
         {projectId: $scope.project_id},
         function (data) {
           $scope.members = data.result;
           //console.log (all_users);
           $scope.users = get_user_diff(all_users, $scope.members);
         },
         function (err_res) {
           console.log("Unable to get members for project : " +
                       $scope.project_id);
           $state.go("manage.projects.show", { projectId: $scope.project_id });
         });
     }

     $scope.removeMember = function(userid) {
       var result = confirmDlg.show(
         "Remove member?",
         "The user '" + userid + "' will have no access to the project. " +
           "However, previous timelog entries made by the user will not be " +
           "removed. Do you want to continue?");
       result.then(function () {
         Member.remove(
           {
             projectId: $scope.project_id,
             userId: userid
           },
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };

     $scope.canAdd = function () {
       return $scope.memberAddForm.$dirty && $scope.memberAddForm.$valid;
     };

     $scope.addMember = function () {
       if (!$scope.memberAddForm.$valid) {
         return;
       }
       MemberCollection.save(
         {
           projectId: $scope.project_id
         },
         {
           userid : $scope.member.userid
         },
         function (data) {
           $scope.member = '';
           update ();
         },
         function (err_res) {
           errorDlg.show(err_res);
         });

     };

   }]);

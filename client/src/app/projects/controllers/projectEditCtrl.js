angular.module('ttrack.projects.controllers.edit', [
  'ttrack.utils',
  'ttrack.resources'
]).controller(
  'projectEditCtrl',
  ['$scope', 'Project', '$stateParams', '$state', 'apiErrorDialog',
   'Client',
   function ($scope, Project, $stateParams, $state, errorDlg, Client) {
     var project_id = $stateParams.projectId;
     Project.get(
       {id: project_id},
       function(data) {
         $scope.project = data.result;
         var client_id = $scope.project.client_id;
         Client.get(
           {id: client_id},
           function (data) {
             $scope.client = data.result;
           },
           function (err_res) {
             console.log("Invalid client_id : " + client_id);
           });
       },
       function(err_res) {
         console.log("Invalid project_id : " + project_id);
         $state.go("manage.projects.list");
       }
     );

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.projectEditForm.$dirty && $scope.projectEditForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.projectEditForm.$valid) {
         return;
       }
       Project.save(
         {id: project_id}, $scope.project,
         function (data) {
           $state.go("manage.projects.show", {projectId: project_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         }
       );
     };
   }
  ]);

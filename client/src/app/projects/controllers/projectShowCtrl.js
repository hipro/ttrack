angular.module('ttrack.projects.controllers.show', [
  'ttrack.resources'
]).controller(
  'projectShowCtrl',
  ['$scope', 'Project', '$stateParams', '$state', 'Client',
   function ($scope, Project, $stateParams, $state, Client) {
     var project_id = $stateParams.projectId;
     $scope.activity_limit = 2;
     $scope.member_limit = 2;
     Project.get(
       {id: project_id},
       function (data) {
         $scope.project = data.result;
         var client_id = $scope.project.client_id;
         Client.get(
           {id: client_id},
           function (data) {
             $scope.client = data.result;
           },
           function (err_res) {
             console.log("Invalid client_id : " + client_id);
           });
       },
       function(err_res) {
         console.log("Invalid project_id : " + project_id);
         $state.go("manage.projects.list");
       });
     $scope.changeActivityLimit = function (limit) {
       $scope.activity_limit = limit;
     };
     $scope.changeMemberLimit = function (limit) {
       $scope.member_limit = limit;
     };
   }
  ]);

angular.module('ttrack.projects.controllers.activities.show', [
  'ttrack.resources'
]).controller(
  'activityShowCtrl',
  ['$scope', 'Activity', '$stateParams', '$state',
   function ($scope, Activity, $stateParams, $state) {
     $scope.project_id = $stateParams.projectId;
     var act_name = $stateParams.activityName;
     Activity.get(
       {
         projectId: $scope.project_id,
         activityName: act_name
       },
       function (data) {
         $scope.activity = data.result;
       },
       function (err_res) {
         console.log("Invalid activity name : " + act_name);
         $state.go("manage.activities.list", { projectId: $scope.project_id });
       }
     );
   }
  ]);

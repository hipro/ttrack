angular.module('ttrack.projects.manage', [
  'ui.router',
  'ttrack.projects.controllers.list',
  'ttrack.projects.controllers.add',
  'ttrack.projects.controllers.show',
  'ttrack.projects.controllers.edit',
  'ttrack.projects.controllers.activities.list',
  'ttrack.projects.controllers.activities.add',
  'ttrack.projects.controllers.activities.show',
  'ttrack.projects.controllers.activities.edit',
  'ttrack.projects.controllers.members'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
      .state('manage.projects', {
        url: '/projects',
        abstract: true,
        template: '<ui-view/>'
      })
      // ----------- States for managing Project --------------
      // Make this state as default by making the url as '' and its
      // parent as abstract
      .state('manage.projects.list', {
        url: '',
        controller: 'projectListCtrl',
        templateUrl: 'projects/projectList.tpl.html'
      })
      .state('manage.projects.add', {
        url: '/add',
        controller: 'projectAddCtrl',
        templateUrl: 'projects/projectAdd.tpl.html'
      })
      .state('manage.projects.show', {
        url: '/show/:projectId',
        controller: 'projectShowCtrl',
        templateUrl: 'projects/projectShow.tpl.html'
      })
      .state('manage.projects.edit', {
        url: '/edit/:projectId',
        controller: 'projectEditCtrl',
        templateUrl: 'projects/projectEdit.tpl.html'
      })
      // ----------- States for managing Activities --------------
      .state('manage.activities', {
        parent: 'manage.projects',
        url: '/project/:projectId/activities',
        abstract: true,
        template: '<ui-view/>'
      })
      .state('manage.activities.list', {
        url: '',
        controller: 'activityListCtrl',
        templateUrl: 'projects/activityList.tpl.html'
      })
      .state('manage.activities.add', {
        url: '/add',
        controller: 'activityAddCtrl',
        templateUrl: 'projects/activityAdd.tpl.html'
      })
      .state('manage.activities.show', {
        url: '/show/:activityName',
        controller: 'activityShowCtrl',
        templateUrl: 'projects/activityShow.tpl.html'
      })
      .state('manage.activities.edit', {
        url: '/edit/:activityName',
        controller: 'activityEditCtrl',
        templateUrl: 'projects/activityEdit.tpl.html'
      })
      // ----------- States for managing Members --------------
      .state('manage.members', {
        parent: 'manage.projects',
        url: '/project/:projectId/members',
        controller: 'memberCtrl',
        templateUrl: 'projects/members.tpl.html'
      });

    }
  ]);

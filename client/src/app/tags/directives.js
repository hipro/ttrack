angular.module('ttrack.tags.directives', [
  'ttrack.resources'
])

  .directive(
    'uniqueTagName',
    ['Tag', function(Tag) {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                Tag.get(
                  { name: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity('uniqueTagName', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity('uniqueTagName', true);
                  });
              }
            });
          });
        }
      };
    }])
  .directive(
    'uniqueTagNameExclude',
    ['Tag', '$parse', function(Tag, $parse) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function(event) {
            scope.$apply(function () {
              var viewValue = element.val();
              var modelGetter = $parse(attrs.uniqueTagNameExclude);
              var tag_name = modelGetter(scope);
              if (viewValue && tag_name) {
                if (viewValue.toLowerCase() != tag_name.toLowerCase()) {
                  // Here both viewValue and the exclude tag_name is
                  // available. we try to check if a tag is unique
                  // only if the given tag_name is different from the
                  // exclude tag_name
                  Tag.get(
                    { name: viewValue },
                    function (data) {
                      ngModelCtrl.$setValidity('uniqueTagNameExclude', false);
                    },
                    function (err_res) {
                      ngModelCtrl.$setValidity('uniqueTagNameExclude', true);
                    });
                }
                else {
                  ngModelCtrl.$setValidity('uniqueTagNameExclude', true);
                }
              }
            });
          });
        }
      };
    }]);

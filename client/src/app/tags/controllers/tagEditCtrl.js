angular.module('ttrack.tags.controllers.edit', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.tags.directives'
]).controller(
  'tagEditCtrl',
  ['$scope', 'Tag', '$stateParams', '$state', 'apiErrorDialog',
   function ($scope, Tag, $stateParams, $state, errorDlg) {
     var tag_name = $stateParams.tagName;
     Tag.get(
       { name: tag_name },
       function(data) {
         $scope.tag = data.result;
         $scope.tag.new_name = tag_name;
       },
       function(err_res) {
         console.log("Invalid tag_name : " + tag_name);
         $state.go("manage.tags.list");
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.tagEditForm.$dirty && $scope.tagEditForm.$valid;
     };

     $scope.cancel = function () {
       $state.go("manage.tags.list");
     };

     $scope.save = function () {
       if (!$scope.tagEditForm.$valid) {
         return;
       }
       // $scope.tag will contain both name and new_name so we
       // construct a new tag object and pass it for PUT
       var tag = {};
       tag.name = $scope.tag.new_name;
       Tag.save(
         { name: tag_name }, tag,
         function (data) {
           $state.go("manage.tags.show", {tagName: tag.name});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }
  ]);

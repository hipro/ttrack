angular.module('ttrack.tags.controllers.add', [
  'ttrack.utils',
  'ttrack.resources',
  'ttrack.tags.directives'
]).controller(
  'tagAddCtrl',
  ['$scope', 'TagCollection', '$state', 'apiErrorDialog',
   function ($scope, TagCollection, $state, errorDlg) {

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.cancel = function () {
       $state.go("manage.tags.list");
     };

     $scope.save = function () {
       if (!$scope.tagAddForm.$valid) {
         return;
       }
       TagCollection.save(
         $scope.tag,
         function (data) {
           $state.go("manage.tags.list");
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return $scope.tagAddForm.$dirty && $scope.tagAddForm.$valid;
     };

   }]);

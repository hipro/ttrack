angular.module('ttrack.providers', [
  'ttrack.resources'
  ])

  .service(
    'userInfo',
    ["UIUserSession", "$q", function(UIUserSession, $q) {

      var permissions;
      var member_projects;
      var member_clients;
      var role_id;
      var role_name;
      var userid;
      var name;

      var deferred = $q.defer();

      this.initializedData = function () {
        // The promise of the deferred returned in this method will be
        // resolved only when the UIUserSession resource gets
        // resolved. So, this method is basically to ensure that we
        // have a valid data in a UI State and it should be used in
        // 'resolve' parameter of UI Router's state config
        return deferred.promise;
      };

      UIUserSession.get(
        function (data) {
          permissions = data.result.permissions;
          member_projects = data.result.projects;
          member_clients = data.result.clients;
          role_id = data.result.role_id;
          role_name = data.result.role_name;
          userid = data.result.userid;
          name = data.result.real_name;
          deferred.resolve(true);
        },
        function (err_res) {
          console.log("Error occurred while get UIUserSession : " + err_res);
          deferred.reject("Unable to get UIUserSession");
        }
      );


      this.getID = function () {
        return userid;
      };

      this.getName = function () {
        return name;
      };

      this.getTimelogMemberProjects = function () {
        // TBD: This should return projects if the current user has
        // timesheet.add permission on them
        return member_projects;
      };

      this.getReportsMemberProjects = function () {
        return member_projects;
      };

      this.getReportsClients = function () {
        return member_clients;
      };

      this.getReportsUsers = function () {

      };

      this.hasAnyManagePermission = function () {
        return (this.hasUserManagePermission() ||
                this.hasRoleManagePermission() ||
                this.hasClientManagePermission() ||
                this.hasProjectManagePermission() ||
                this.hasTagManagePermission());

      };

      this.hasUserManagePermission = function () {
        return (permissions &&
                ($.inArray('users.manage', permissions.global) > -1 ||
                 $.inArray('ALL_PERMISSIONS', permissions.global) > -1));
      };

      this.hasRoleManagePermission = function () {
        return (permissions &&
                ($.inArray('roles.manage', permissions.global) > -1 ||
                 $.inArray('ALL_PERMISSIONS', permissions.global) > -1));
      };

      this.hasClientManagePermission = function () {
        return (permissions &&
                ($.inArray('clients.manage', permissions.global) > -1 ||
                 $.inArray('ALL_PERMISSIONS', permissions.global) > -1));
      };

      this.hasProjectManagePermission = function () {
        return (permissions &&
                ($.inArray('projects.manage', permissions.global) > -1 ||
                 $.inArray('ALL_PERMISSIONS', permissions.global) > -1));
      };

      this.hasTagManagePermission = function () {
        return (permissions &&
                ($.inArray('tags.manage', permissions.global) > -1 ||
                 $.inArray('ALL_PERMISSIONS', permissions.global) > -1));
      };


    }]);

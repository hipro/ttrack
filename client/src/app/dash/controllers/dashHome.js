angular.module('ttrack.dashboard.home', [
  'tc.chartjs',
  'ttrack.resources',
  'ttrack.reports.providers'
])

  .controller(
    "dashHomeCtrl",
    ["$scope", "TimeSheetCollection", "UserProfile", "UserReport",
     'rptColorCodeTable', 'rptUtils',
     function ($scope, TimeSheetCollection, UserProfile, UserReport,
               rptColorCodeTable, rptUtils) {
       $scope.ranges = rptUtils.getSelectRangeOptions();
       $scope.filter_range = $scope.ranges[0];
       $scope.records_available = false;
       $scope.projects = [];

       $scope.projects_available = function () {
         return $scope.projects.length > 0;
       };

       UserProfile.get (
         function (data) {
           $scope.projects = data.result.projects;
         },
         function (err_res) {
           console.log("Error occurred while getting projects : " + err_res);
         });

       TimeSheetCollection.get(
         {
           page: 1
         },
         function (data) {
           $scope.timelogs = [];
           var date_wise = {date: '', logs: []};
           var first = true;
           for (var index in data.result) {
             var te = data.result[index];
             if (date_wise.date != te.date) {
               if (!first) {
                 $scope.timelogs.push(date_wise);
                 date_wise = {date: '', logs: []};
               }
               first = false;
               date_wise.date = te.date;
             }
             date_wise.logs.push(te);
           }
           if (date_wise.date) {
             $scope.timelogs.push(date_wise);
           }
           if ($scope.timelogs.length > 0) {
             $scope.records_available = true;
           }
         }
       );

       $scope.update = function () {
         $scope.stats_available = false;
         var params = {};
         params.id = $scope.userInfo.getID();
         params.start_date = $scope.filter_range.start_date;
         params.end_date = $scope.filter_range.end_date;
         params.time_key = $scope.filter_range.time_key;

         UserReport.get(
           params,
           function (data) {
             if (data.result.total > 0) {
               $scope.stats_available = true;
               constructGraphicalData(data.result);
             }
           }
         );
       };

     function constructGraphicalData(data) {
       var colorCode = rptColorCodeTable;
       $scope.projectChartData = [];
       $scope.clientChartData = [];
       $scope.tagChartData = [];
       var chart_data;
       for (var d in data.projects) {
         chart_data = {};
         chart_data.value = data.projects[d].total;
         chart_data.color = colorCode.getColor();
         chart_data.label = d;
         chart_data.style = colorCode.getStyle();
         $scope.projectChartData.push(chart_data);
       }

       for (d in data.clients) {
         chart_data = {};
         chart_data.value = data.clients[d];
         chart_data.color = colorCode.getColor();
         chart_data.label = d;
         chart_data.style = colorCode.getStyle();
         $scope.clientChartData.push(chart_data);
       }
       for (d in data.tags) {
         chart_data = {};
         chart_data.value = data.tags[d];
         chart_data.color = colorCode.getColor();
         chart_data.label = d;
         chart_data.style = colorCode.getStyle();
         $scope.tagChartData.push(chart_data);
       }
     }

     $scope.getTimeString = function (minutes) {
       if (minutes < 60) {
         return minutes + " minutes";
       }
       return parseFloat(minutes/60).toFixed(2) + " hours";
     };

     $scope.getDate = function (date_str) {
       var dt = new Date(date_str);
       return dt.toDateString();
     };

     $scope.getShortDescription = function (description) {
       if (description) {
         return description.split('\n')[0];
       }
       return "";
     };

     $scope.update();


     }]);
